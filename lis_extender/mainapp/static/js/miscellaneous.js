var csrfToken;

if(document.getElementsByName('csrfmiddlewaretoken').length > 0){
    csrfToken = document.getElementsByName('csrfmiddlewaretoken')[0].value;
}

document.querySelector('body').addEventListener('click', deleteEntity);

function deleteEntity(event){
    const button = event.target;
    if(button.classList.contains('delete-button')){
        event.preventDefault();

        const modal = button.parentElement.parentElement.parentElement;
        const rowId = modal.id.split("_").slice(1, 2)[0];
        const row = document.getElementById("row_" + rowId);

        const response = fetch(button.href,
            {
            method: "POST",
            headers: {
                'X-CSRFToken': csrfToken,
                'Content-Type': "application/json;charset=utf-8",
                'Accept': "application/json"
            },
    })
    modal.remove();
    row.remove();
}
};

function sendFormByTab(event, formID, fieldToCheckID){
    if (event.code == 'Tab'){
        event.preventDefault();
        let frm = document.getElementById(formID);
        if(typeof frm == null){
            frm = document.querySelector('form');
        }
        let inp = document.getElementById(fieldToCheckID);
        if (inp.value.length > 0) {
            frm.submit();
        }
    }
};


function setCaretPosition(elemId, caretPos) {
    let elem = document.getElementById(elemId);
    if(elem != null) {
        if(elem.createTextRange) {
            let range = elem.createTextRange();
            range.move('character', caretPos);
            range.select();
        }
        else {
            if(elem.selectionStart) {
                elem.focus();
                elem.setSelectionRange(caretPos, caretPos);
            }
            else
                elem.focus();
        }
    }
};


function loadPageSection(selector, callback) {
    const url = window.location.href
    if (typeof selector !== 'string') {
    throw new Error('Invalid selector selector: ', selector);
  }

  const xhr = new XMLHttpRequest();
  let finished = false;
  const targetElement = document.querySelector(selector);
  xhr.onabort = xhr.onerror = function() {
    finished = true;
    console.log(xhr.statusText);
  };

  xhr.onload = function() {
    if (this.status === 200 && !finished) {
      finished = true;
      let section;
      try {
        section = xhr.responseXML.querySelector(selector);
        if(!targetElement.isEqualNode(section)){
            targetElement.innerHTML = section.innerHTML;
        if(callback != undefined){
            callback();
        }
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  xhr.open('GET', url);
  xhr.responseType = 'document';
  xhr.send();
};

function showMessages(msgLevel, msgText){
    UIkit.notification({
    message: msgText,
    status: msgLevel,
    pos: 'top-center',
    timeout: 20000
    }
   );
 };

function messageIterator(messagesObject){
    for (let [key, value] of Object.entries(messagesObject)) {
        showMessages(value, key);
}
};

function initDatePicker(txtFieldElement){
    let datepicker = new TheDatepicker.Datepicker(txtFieldElement);
    datepicker.options.setInputFormat('j.n.Y');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Monday, 'Пн');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Tuesday, 'Вт');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Wednesday, 'Ср');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Thursday, 'Чт');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Friday, 'Пт');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Saturday, 'Сб');
    datepicker.options.translator.setDayOfWeekTranslation(TheDatepicker.DayOfWeek.Sunday, 'Вс');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.January, 'Январь');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.February, 'Февраль');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.March, 'Март');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.April, 'Апрель');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.May, 'Май');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.June, 'Июнь');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.July, 'Июль');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.August, 'Август');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.September, 'Сентябрь');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.October, 'Октябрь');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.November, 'Ноябрь');
    datepicker.options.translator.setMonthTranslation(TheDatepicker.Month.December, 'Декабрь');
    datepicker.render();
};

function initCalendars(){
    const datePickers = document.querySelectorAll('.datepicker-here');
    if(datePickers.length > 0){
        datePickers.forEach(function(txtElement){
            initDatePicker(txtElement);
        });
    }
};

function initJSSelect(selectElements, search){
    let options = {
        locale: "ru",
        search: search,
        deselect: search,
        multiSelectAll: search,
    }
    tail.select(selectElements, options)
};

function JSSelectWOSearch(selectElements){
    initJSSelect(selectElements, false);
};

function JSSelectWithSearch(selectElements){
    initJSSelect(selectElements, true)
};

function initJSSelects(){
    const selectElements = document.querySelectorAll('.js-select');
    const selectSearchElements = document.querySelectorAll('.js-select-search');
    if(selectElements.length > 0){
            JSSelectWOSearch(selectElements);
    }
    if(selectSearchElements.length > 0){
            JSSelectWithSearch(selectSearchElements);
    }
};

function backButtonActivation(){
    let backButton = document.querySelector('.back-button');
    if(backButton){
        backButton.addEventListener('click', function(e){
            e.preventDefault();
            window.history.back;
        })
    }
};

function colorErrorFields(){
    document.querySelectorAll('.input-error').forEach(function(oneError){
        oneError.parentElement.querySelector('input').classList.add('uk-form-danger');
    })
};


function filterElements(inputSelector, elementsContainer){
    document.querySelector(inputSelector).addEventListener('keyup', function(event){
        const text = event.target.value.toLowerCase();
        const elements = document.querySelector(elementsContainer).children;

        for(let i=0; i < elements.length; i++){
            let row = elements[i];
            let rowText = row.innerText;
            if(rowText.toLowerCase().indexOf(text) != -1){
                row.style.display = '';
            } else {
                row.style.display = 'none';
            }
        }
    })
}

document.addEventListener('DOMContentLoaded', function(){
    initCalendars();
    initJSSelects();
    backButtonActivation();
    colorErrorFields();
});
