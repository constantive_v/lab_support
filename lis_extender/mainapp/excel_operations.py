import datetime

from openpyxl import Workbook, load_workbook
from io import BytesIO


class DoubleCodeError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return ', '.join(self.value)


class ExcelConverter:
    header = None
    sheet_name = 'Лист 1'

    def __init__(self, excel_file):
        self._excel_file = excel_file
        self._input_list = self._excel_to_list()
        self.output_excel = BytesIO()

    def _resize_cols(self, worksheet):
        dims = {}
        for row in worksheet.rows:
            for cell in row:
                if cell.value:
                    dims[cell.column_letter] = max((dims.get(cell.column_letter, 0), len(str(cell.value))))
        for col, value in dims.items():
            worksheet.column_dimensions[col].width = value

    def _excel_to_list(self):
        list_to_return = list()
        wb = load_workbook(self._excel_file.file)
        ws = wb.worksheets[0]
        for row in ws:
            if row[0] and row[1]:
                temp_list = [cell.value for cell in row]
                list_to_return.append(temp_list)
        return list_to_return

    def _list_to_excel(self, list_of_lists):
        wb = Workbook()
        ws = wb.active
        if self.header:
            ws.append(self.header)
        ws.title = self.sheet_name
        for lst in list_of_lists:
            ws.append(lst)
        self._resize_cols(ws)
        wb.save(self.output_excel)

    @staticmethod
    def _check_list_for_uniqueness(codes_list):
        uniq_codes = set([x for x in codes_list if codes_list.count(x) > 1])
        if len(uniq_codes) > 0:
            raise DoubleCodeError(uniq_codes)


def input_name_to_output_name(file_name):
    file_name = file_name.name
    file_name = file_name.replace(' ', '_')
    reversed_string = file_name[::-1]
    dot_index = reversed_string.index('.') + 1
    without_extension = (reversed_string[dot_index::])[::-1]
    dte = datetime.datetime.now().strftime('%H.%M.%S')
    output_name = f'{without_extension}_{dte}.xlsx'
    return output_name
