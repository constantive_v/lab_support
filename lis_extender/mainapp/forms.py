from django import forms
from django.contrib.auth.models import User, Group

from .models import HSLink, Profile, Position


class HSLinkUpdateForm(forms.ModelForm):
    name = forms.CharField(label='Название ресурса', widget=forms.TextInput(attrs={'class': 'uk-input'}))
    logo = forms.ImageField(label='Логотип ресурса', required=False,
                            widget=forms.FileInput(attrs={'class': 'form-control-file'}))
    link = forms.URLField(label='Ссылка на ресурс', widget=forms.TextInput(attrs={'class': 'uk-input',
                                                                                  'autocomplete': 'off'}))
    description = forms.CharField(label='Описание ресурса', widget=forms.Textarea(attrs={'class': 'uk-input',
                                                                                         'rows': '5'}))
    weight = forms.CharField(label='Вес ссылки', initial='1', widget=forms.NumberInput(attrs={'class': 'uk-input'}))
    is_active = forms.BooleanField(label='Активный', initial=True, required=False,
                                   widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox'}))

    class Meta:
        model = HSLink
        fields = [
            'name',
            'logo',
            'link',
            'description',
            'weight',
            'is_active'
        ]


class UserForm(forms.ModelForm):
    username = forms.CharField(label='Логин', min_length=5, max_length=150, widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    first_name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    last_name = forms.CharField(label='Фамилия', widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'validate', 'autocomplete': 'off',
    }))
    groups = forms.ModelMultipleChoiceField(label='В группах', required=False,
                                            queryset=Group.objects.all(),
                                            widget=forms.SelectMultiple(attrs={
                                            }))
    password = forms.CharField(label='Пароль', min_length=8, widget=forms.PasswordInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    password_confirmation = forms.CharField(label='Подтверждение пароля', min_length=8,
                                            widget=forms.PasswordInput(attrs={
                                                'class': 'validate',
                                                'autocomplete': 'off'
                                            }))

    def clean(self):
        form_data = self.cleaned_data
        if form_data['password'] != form_data['password_confirmation']:
            self.add_error('password', 'Пароли не совпадают')


    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'groups',
        ]


class ProfileForm(forms.ModelForm):
    position = forms.ModelChoiceField(required=False, label='Должность', queryset=Position.objects.all(),
                                      widget=forms.Select(attrs={}))

    class Meta:
        model = Profile
        fields = [
            'position'
        ]


class UserCreateForm(forms.Form):
    username = forms.CharField(label='Логин', min_length=5, max_length=150, widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    first_name = forms.CharField(label='Имя', widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    last_name = forms.CharField(label='Фамилия', widget=forms.TextInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={
        'class': 'validate', 'autocomplete': 'off',
    }))
    groups = forms.ModelMultipleChoiceField(label='В группах', required=False,
                                            queryset=Group.objects.all(),
                                            widget=forms.SelectMultiple(attrs={
                                            }))
    position = forms.ModelChoiceField(required=False, label='Должность', queryset=Position.objects.all(),
                                      widget=forms.Select(attrs={}))
    password = forms.CharField(label='Пароль', min_length=8, widget=forms.PasswordInput(attrs={
        'class': 'validate', 'autocomplete': 'off'
    }))
    password_confirmation = forms.CharField(label='Подтверждение пароля', min_length=8,
                                            widget=forms.PasswordInput(attrs={
                                                'class': 'validate',
                                                'autocomplete': 'off'
                                            }))

    def clean(self):
        form_data = self.cleaned_data
        if form_data['password'] != form_data['password_confirmation']:
            self.add_error('password', 'Пароли не совпадают')


class PositionForm(forms.ModelForm):
    name = forms.CharField(label='Название должности', widget=forms.TextInput(attrs={
        'class': 'validate'
    }))

    class Meta:
        model = Position
        fields = [
            'name'
        ]
