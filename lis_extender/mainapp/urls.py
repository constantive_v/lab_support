from django.urls import path
from django.views.generic import ListView, DetailView, DeleteView
from .models import HSLink
from .views import CreateHE, UpdateHE, DeleteHE

urlpatterns = [
    path('', ListView.as_view(queryset=HSLink.objects.filter(is_active=True), template_name='mainapp/home.html'), name='home'),
    path('hl/', ListView.as_view(queryset=HSLink.objects.all()), name='home_elements'),
    path('hl/create/', CreateHE.as_view(), name='home_element_create'),
    path('hl/<slug>/', DetailView.as_view(model=HSLink), name='home_element_detail'),
    path('hl/<slug>/edit/', UpdateHE.as_view(), name='home_element_edit'),
    path('hl/<slug>/delete/', DeleteHE.as_view(), name='home_element_delete'),
    path('users/', ListView.as_view(), name='users'),
    path('users/create/', DetailView.as_view(), name='user_create'),
    path('users/<int:pk>/', DetailView.as_view(), name='user'),
    path('users/<int:pk>/edit/', DetailView.as_view(), name='user_edit'),
    path('users/<int:pk>/delete/', DeleteView.as_view(), name='user_delete'),
    path('positions/<int:pk>/', DetailView.as_view(), name='position'),
    path('positions/<int:pk>/edit/', DetailView.as_view(), name='position_edit'),
    path('positions/<int:pk>/delete/', DeleteView.as_view(), name='position_delete'),
]
