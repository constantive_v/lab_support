import json
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class LogEvent(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)
    data = models.TextField(default=str({}))

    def save_data(self, data):
        self.data = json.dumps(data)

    def get_data(self):
        return json.loads(self.data)

    class Meta:
        ordering = ('-timestamp',)


class HSLink(models.Model):
    name = models.CharField(max_length=120, verbose_name='Название')
    logo = models.ImageField(upload_to='uploads/home_logos/', verbose_name='Логотип', default='site.jpg')
    link = models.URLField(unique=True, verbose_name='Ссылка')
    description = models.TextField(verbose_name='Описание')
    slug = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    weight = models.IntegerField(default=1, verbose_name='Вес ссылки')
    is_active = models.BooleanField(default=True, verbose_name='Активна')
    create_date = models.DateTimeField(default=timezone.now)
    edit_date = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('home_element_detail', kwargs={'slug': self.slug})

    def get_edit_url(self):
        return reverse('home_element_edit', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('home_element_delete', kwargs={'slug': self.slug})

    class Meta:
        ordering = ('weight', 'name',)


class LaboratoryInfo(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=50)
    logo = models.ImageField(null=True, blank=True, upload_to='logo/')


class Position(models.Model):
    name = models.CharField(max_length=120, unique=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    position = models.ForeignKey(to=Position, on_delete=models.SET_DEFAULT, null=True, blank=True, default=None)
