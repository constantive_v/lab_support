from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.http import Http404, HttpResponse
from django.views.generic import FormView, UpdateView, DeleteView, ListView, CreateView
from django.db.transaction import atomic

from mainapp.models import LogEvent
from .forms import HSLinkUpdateForm, UserForm, ProfileForm, UserCreateForm, PositionForm
from .models import HSLink, Position
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User


class AjaxDeleteView(DeleteView):

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse()

class CreateHE(PermissionRequiredMixin, CreateView):
    form_class = HSLinkUpdateForm
    template_name = 'mainapp/add_home_element.html'
    permission_required = 'mainapp.add_hslink'
    permission_denied_message = 'Для получения доступа обратитесь в IT отдел'

    def get_success_url(self):
        return reverse('home_elements')


class UpdateHE(PermissionRequiredMixin, UpdateView):
    model = HSLink
    form_class = HSLinkUpdateForm
    template_name = "mainapp/home_element_edit.html"
    queryset = HSLink.objects.all()
    permission_required = 'mainapp.change_hslink'
    permission_denied_message = 'Для получения доступа обратитесь в IT отдел'

    def get_success_url(self):
        return reverse('home_elements')

class DeleteHE(PermissionRequiredMixin, AjaxDeleteView):
    model = HSLink
    permission_required = 'mainapp.delete_hslink'
    permission_denied_message = 'Для получения доступа обратитесь в IT отдел'


class ChangeLogMixin:
    log_connector_class = None

    def make_log(self, attribs, original_object):
        log_data = {}
        for attrib in attribs:
            if getattr(self.object, attrib) == getattr(original_object, attrib):
                log_data[attrib] = 'Без изменений'
            else:
                value = getattr(original_object, attrib)
                log_data[attrib] = ' ' if value is None else str(value)
        if len(self.request.user.first_name) > 0 and len(self.request.user.last_name) > 0:
            username = self.request.user.get_full_name()
        else:
            username = str(self.request.user)
        log_data['author'] = username
        log_line = LogEvent()
        log_line.save_data(log_data)
        log_line.save()
        log_entry_connector = self.log_connector_class(obj_instance=self.object, event=log_line)
        log_entry_connector.save()


class FilteredListView(ListView):
    filterset_class = None

    def get_queryset(self):
        queryset = super().get_queryset()
        self.filterset = self.filterset_class(self.request.GET, queryset=queryset)
        return self.filterset.qs.distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filterset'] = self.filterset
        return context


class UsersListView(PermissionRequiredMixin, FilteredListView):
    permission_required = 'auth.view_user'
    template_name = 'mainapp/credentials_list.html'
    queryset = User.objects.all()
    paginate_by = 15


class PositionsListView(PermissionRequiredMixin, FilteredListView):
    permission_required = 'auth.view_user'
    template_name = 'mainapp/credentials_list.html'
    queryset = Position.objects.all()
    paginate_by = 15


class UserCreateView(PermissionRequiredMixin, FormView):
    permission_required = 'auth.add_user'
    template_name = 'mainapp/user_create.html'
    form_class = UserCreateForm

    def form_valid(self, form):
        user_data = form.cleaned_data
        del user_data['password_confirmation']
        groups = user_data.pop('groups')
        position = user_data.pop('position')
        user = User.objects.create_user(**user_data)
        user.is_active = True
        for group in groups:
            user.groups.add(group)
        user.save()
        user.profile.position = position
        user.profile.save()


@permission_required(perm='auth.change_user')
@atomic
def update_user(request, pk):
    try:
        user = get_object_or_404(User, pk=pk)
    except User.DoesNotExist:
        raise Http404()
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=user)
        profile_form = ProfileForm(request.POST, instance=user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            profile_form.save()
            user_form.save()
            if request.GET.get('next') is not None:
                success_url = request.GET.get('next')
            else:
                success_url = reverse('customers')
            return redirect(success_url)
    else:
        user_form = UserForm(instance=user)
        profile_form = ProfileForm(instance=user.profile)
    return render(request, 'mainsapp/customer_form.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })


class PositionCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'mainapp.add_position'
    model = Position
    form_class = PositionForm


class PositionEditView(PermissionRequiredMixin, UpdateView):
    permission_required = 'mainapp.change_position'
    model = Position
    form_class = PositionForm