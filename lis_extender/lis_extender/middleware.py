from django.http import HttpResponseRedirect


class AuthRequiredMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.user.is_authenticated:
            return response
        else:
            if request.path[:14] == '/account/login':
                return response
            # elif request.path == '/account/password_reset':
            #     return response
            else:
                if request.path[:15] == '/account/logout':
                    return HttpResponseRedirect(f'/account/login/')
                else:
                    return HttpResponseRedirect(f'/account/login/?next={request.path}')
