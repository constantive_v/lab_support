from django.conf import settings
import os
from datetime import datetime

LOG_PATH = settings.LOG_PATH


def write_to_log(log_name, error):
    if not os.path.isdir(LOG_PATH):
        os.makedirs(LOG_PATH)
    LOG = os.path.join(LOG_PATH, log_name)
    with open(LOG, mode='a', encoding='utf8') as log:
        log_row = datetime.now().strftime('%d.%m.%YT%H:%M:%S') + ' File: ' + os.path.abspath(
            __file__) + ' Class ' + 'Command' + ' Function: ' + 'handle' + ' Exception: ' + str(error) + '\n'
        log.write(log_row)


def exceptions_log_decorator(func, log_file):
    
    return exceptions_log_decorator