from django.conf import settings
import functools
import logging
import os
import inspect

LOG_PATH = settings.LOG_PATH


def create_logger():
    logger = logging.getLogger("example_logger")
    logger.setLevel(logging.INFO)
    if not os.path.isdir(LOG_PATH):
        os.makedirs(LOG_PATH)
    fh = logging.FileHandler(os.path.join(LOG_PATH, 'converters.log'))
    fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger

def get_class_that_defined_method(meth):
    if inspect.ismethod(meth):
        for cls in inspect.getmro(meth.__self__.__class__):
           if cls.__dict__.get(meth.__name__) is meth:
                return cls
        meth = meth.__func__
    if inspect.isfunction(meth):
        cls = getattr(inspect.getmodule(meth),
                      meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0])
        if isinstance(cls, type):
            return cls
    return getattr(meth, '__objclass__', None)


def log_exceptions(func):
    @functools.wraps(func)
    def surrogate(*args, **kwargs):
        logger = create_logger()
        try:
            return func(*args, **kwargs)
        except:
            err = ' Class: ' + get_class_that_defined_method(func).__name__
            err += ' Function: ' + func.__name__
            logger.exception(err)
            raise
    return surrogate
