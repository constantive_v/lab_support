import datetime


class AnalyzesReport:
    def __init__(self, analysis, start_date, final_date=None, group_by=None):
        self.obj = analysis
        self.grouped_sales = None
        if type(start_date) is datetime.date:
            self.start_date = start_date
        else:
            self.start_date = start_date.date()
        if type(final_date) is datetime.date:
            self.final_date = final_date
        elif final_date is None:
            self.final_date = datetime.datetime.now().date()
        else:
            self.final_date = final_date.date()
        self.group_by = group_by
        self.filtered_sales = self.obj.sales.filter(creation_date__range=(start_date, final_date))

    def add_month(self, start_date):
        month = start_date.month
        year = start_date.year + month // 12
        month = month % 12 + 1
        # day = min(start_date.day, calendar.monthrange(year, month)[1])  # если надо брать по месяцу от даты, а не от первого числа
        day = 1
        return datetime.date(year, month, day)

    def add_week(self, start_date):
        monday_date = start_date - datetime.timedelta(days=start_date.weekday())
        sunday_date = monday_date + datetime.timedelta(days=7)
        return sunday_date

    def add_day(self, start_date):
        return start_date + datetime.timedelta(days=1)

    def add_nothing(self):
        return self.final_date

    def get_end_date(self, start_date):
        if self.group_by == 'week':
            end_date = self.add_week(start_date)
        elif self.group_by == 'month':
            end_date = self.add_month(start_date)
        elif self.group_by == 'day':
            end_date = self.add_day(start_date)
        else:
            end_date = self.add_nothing()
        return end_date

    def group_sales(self):
        sales_by_periods = []
        if self.start_date >= self.final_date and self.group_by != 'day':
            raise ValueError
        start_date = self.start_date
        while start_date < self.final_date:
            end_date = self.get_end_date(start_date=start_date)
            if end_date > self.final_date:
                end_date = self.final_date
            sales_in_period = self.filtered_sales.filter(creation_date__range=(start_date, end_date))
            count_analysis = sales_in_period.count()
            sales_by_periods.append({'start_date': start_date, 'end_date': end_date,
                                     'analysis_count': count_analysis})
            start_date = end_date
        self.grouped_sales = sales_by_periods
