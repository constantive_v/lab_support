from django.db import models
from django.urls import reverse
from mainapp.models import LogEvent
import uuid


class TimeUnits(models.Model):
    name = models.CharField(max_length=5, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class MeasureUnit(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50, unique=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Analyzer(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('Analyzer', kwargs={'pk': self.id})


class Biomaterial(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('Biomaterial', kwargs={'pk': self.id})

    class Meta:
        ordering = ('name',)


class Container(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('Container', kwargs={'pk': self.id})

    class Meta:
        ordering = ('name',)


class AnalysisGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('code',)


class Analysis(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=255)
    group = models.ForeignKey(
        to=AnalysisGroup,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    link = models.URLField(blank=True, null=True)
    notes = models.TextField(default='')
    take_instructions = models.TextField(default='')
    store_conditions = models.TextField(default='')
    ready_for = models.CharField(max_length=10, blank=True, null=True)
    tags = models.TextField(default='')
    extra_appointment_days = models.IntegerField(blank=True, null=True)
    extra_appointment_units = models.ForeignKey(to=TimeUnits, on_delete=models.SET_NULL, blank=True, null=True,
                                                related_name='extra_appointment_units')
    stability_8 = models.IntegerField(blank=True, null=True)
    stability_8_unit = models.ForeignKey(to=TimeUnits, on_delete=models.SET_NULL, blank=True, null=True,
                                         related_name='stability_8_unit')
    stability_20 = models.IntegerField(blank=True, null=True)
    stability_20_unit = models.ForeignKey(to=TimeUnits, on_delete=models.SET_NULL, blank=True, null=True,
                                          related_name='stability_20_unit')
    analysis_method = models.TextField(default='Нет данных')
    reagent = models.FileField(upload_to='uploads/reagents/', blank=True, null=True, default=None)
    results_example = models.FileField(upload_to='uploads/results/', blank=True, null=True, default=None)
    clinical_meaning = models.TextField(default='Нет данных')
    influental_factors = models.TextField(default='Нет данных')
    get_ready = models.TextField(default='Нет данных')
    additional_biomaterials = models.ManyToManyField(to=Biomaterial)
    summary = models.TextField(default='')

    def __str__(self):
        return f'{self.code} {self.name}'

    def __eq__(self, other):
        if isinstance(other, Analysis):
            return (self.link == other.link and
                    self.notes == other.notes and
                    self.take_instructions == other.take_instructions and
                    self.store_conditions == other.store_conditions and
                    self.extra_appointment_days == other.extra_appointment_days and
                    self.extra_appointment_units == other.extra_appointment_units and
                    self.stability_8 == other.stability_8 and
                    self.stability_8_unit == other.stability_8_unit and
                    self.stability_20 == other.stability_20 and
                    self.stability_20_unit == other.stability_20_unit and
                    self.analysis_method == other.analysis_method and
                    self.clinical_meaning == other.clinical_meaning and
                    self.influental_factors == other.influental_factors and
                    self.additional_biomaterials.all() == other.additional_biomaterials.all()
                    )
        return NotImplemented

    def get_absolute_url(self):
        return reverse('Analysis', kwargs={'pk': self.id})

    def get_edit_url(self):
        return reverse('Analysis edit', kwargs={'pk': self.pk})

    def get_history_url(self):
        return reverse('Analysis history', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('group__name', 'code',)


class Blank(models.Model):
    code = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=255, default='')
    blank = models.FileField(upload_to='uploads/blanks/')
    slug = models.SlugField(default=uuid.uuid4, editable=False, unique=True)
    analyzes = models.ManyToManyField(to=Analysis)

    def __str__(self):
        return f'{self.code} {self.name}'

    def get_absolute_url(self):
        return reverse('Blank', kwargs={'slug': self.slug})

    def get_edit_url(self):
        return reverse('Edit blank', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('Delete blank', kwargs={'slug': self.slug})

    class Meta:
        ordering = ('code',)


class Test(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, default='')
    short_name = models.CharField(max_length=255, default='')
    alternative_name = models.CharField(max_length=255, default='')
    measuring_units = models.ForeignKey(to=MeasureUnit, on_delete=models.SET_NULL, blank=True, null=True)
    references = models.TextField(default='Нет данных')
    is_technical = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('Test edit', kwargs={'pk': self.id})

    def get_edit_url(self):
        return reverse('Test edit', kwargs={'pk': self.id})

    class Meta:
        ordering = ('name',)


class AnalysisTests(models.Model):
    analysis = models.ForeignKey(to=Analysis, on_delete=models.CASCADE, related_name='tests')
    test = models.ForeignKey(to=Test, on_delete=models.CASCADE, related_name='test_analysis')


class BiomaterialSamplingGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    biomaterial = models.ForeignKey(to=Biomaterial, on_delete=models.CASCADE)
    container = models.ForeignKey(to=Container, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name


class AnalysisSamplingGroup(models.Model):
    analysis = models.ForeignKey(to=Analysis, on_delete=models.CASCADE)
    bsg = models.ForeignKey(to=BiomaterialSamplingGroup, on_delete=models.CASCADE)


class ProfileToSimple(models.Model):
    profile = models.ForeignKey(to=Analysis, on_delete=models.CASCADE, related_name='profile')
    simple_target = models.ForeignKey(to=Analysis, on_delete=models.CASCADE, related_name='simple')


class AnalysisAnalyzers(models.Model):
    analysis = models.ForeignKey(to=Analysis, on_delete=models.CASCADE)
    analyzer = models.ForeignKey(to=Analyzer, on_delete=models.CASCADE)


class AnalysisLog(models.Model):
    obj_instance = models.ForeignKey(to=Analysis, on_delete=models.CASCADE, related_name='analysis')
    event = models.ForeignKey(to=LogEvent, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-event__timestamp',)


class AnalysisMapping(models.Model):
    analysis = models.OneToOneField(to=Analysis, on_delete=models.CASCADE)
    cmd_code = models.CharField(max_length=20, default='', verbose_name='CMD')
    helix_code = models.CharField(max_length=20, default='', verbose_name='Helix')
    invitro_code = models.CharField(max_length=20, default='', verbose_name='InVitro')
    kdl_code = models.CharField(max_length=20, default='', verbose_name='KDL')
    gemotest_code = models.CharField(max_length=20, default='', verbose_name='Гемотест')
    dialab_code = models.CharField(max_length=20, default='', verbose_name='Диалаб')
    citilab_code = models.CharField(max_length=20, default='', verbose_name='Citilab')
    efis_code = models.CharField(max_length=20, default='', verbose_name='Эфис')
    unimed_code = models.CharField(max_length=20, default='', verbose_name='Юнимед')
    labquest_code = models.CharField(max_length=20, default='', verbose_name='LabQuest')
    summary = models.TextField(default='')

    def __str__(self):
        return f'{self.analysis}'

    def __eq__(self, other):
        if isinstance(other, AnalysisMapping):
            is_equal = True
            attrib_codes = [attrib for attrib in dir(other) if not callable(attrib) and 'code' in attrib]
            for attrib in attrib_codes:
                if getattr(self, attrib) != getattr(other, attrib):
                    is_equal = False
            return is_equal
        return NotImplemented

    def get_absolute_url(self):
        return reverse('Mapping', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('analysis__code',)


class AnalysisMappingLog(models.Model):
    obj_instance = models.ForeignKey(to=AnalysisMapping, on_delete=models.CASCADE, related_name='analysismappinglog')
    event = models.ForeignKey(to=LogEvent, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-event__timestamp',)

