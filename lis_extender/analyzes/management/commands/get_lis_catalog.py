import pyodbc
from django.conf import settings
from analyzes.models import MeasureUnit, Analyzer, Biomaterial, Container, AnalysisGroup, Analysis, \
    BiomaterialSamplingGroup, AnalysisSamplingGroup, ProfileToSimple, AnalysisAnalyzers, Test, AnalysisTests
from django.core.management.base import BaseCommand
from lis_extender.decorators import log_exceptions
from django.db.utils import IntegrityError
import multiprocessing

LIS_DB = settings.LIS_DB
LOG_PATH = settings.LOG_PATH

server = LIS_DB.get('server')
database = LIS_DB.get('database')
username = LIS_DB.get('username')
password = LIS_DB.get('password')
IMPORT_CATALOG_LOG = 'import_get_lis_catalog.log'
TARG_WHERE = " where sg.code not in ('178','186','178.5') and len(se.code) < 6 "


class HandleCatalog(multiprocessing.Process):
    table_name = ''
    table_columns = ('id', 'code', 'name', 'removed')
    where = ''
    params = table_columns
    model = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sql = 'select distinct ' + ', '.join(self.table_columns) + ' from ' + self.table_name + ' ' + self.where

    def clear_queryset(self):
        all_objects = self.model.objects.all()
        if len(all_objects) > 0:
            self.model.objects.all().delete()

    @log_exceptions
    def get_catalog_data(self):
        self.catalog_data = []
        cnxn = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
        cursor = cnxn.cursor()
        cursor.execute(self.sql)
        row = cursor.fetchone()
        while row:
            mod_row = {}
            for num, value in enumerate(self.params):
                row_value = row[num]
                try:
                    row_value = int(row_value)
                except ValueError:
                    pass
                except TypeError:
                    pass
                mod_row[value] = row_value
            self.catalog_data.append(mod_row)
            row = cursor.fetchone()


    @log_exceptions
    def object_exists(self, catalog_item):
        obj = self.model.objects.get(id=catalog_item['id'])
        changes = 0
        for key, value in catalog_item.items():
            if key not in ('removed', 'id'):
                if getattr(obj, key) != value:
                    setattr(obj, key, value)
                    changes += 1
        if changes > 0:
            obj.save()

    @log_exceptions
    def create_new_object(self, catalog_item):
        obj = self.model()
        try:
            for key, value in catalog_item.items():
                if key not in ('removed'):
                    setattr(obj, key, value)
            obj.save()
        except IntegrityError:
            pass

    @log_exceptions
    def sync_catalogs_connections(self):
        for catalog_item in self.catalog_data:
            self.create_new_object(catalog_item)

    @log_exceptions
    def sync_catalogs(self):
            for catalog_item in self.catalog_data:
                does_exist = self.model.objects.filter(id=catalog_item['id']).exists()
                if does_exist and catalog_item['removed'] == 0:
                    self.object_exists(catalog_item)
                elif not does_exist and catalog_item['removed'] == 0:
                    self.create_new_object(catalog_item)
                elif does_exist and catalog_item['removed'] == 1:
                    self.model.objects.get(id=catalog_item['id']).delete()


    def run(self):
        self.get_catalog_data()
        self.sync_catalogs()

    def run_with_cleaning(self):
        self.get_catalog_data()
        self.clear_queryset()
        self.sync_catalogs_connections()


class BiomaterialsHandle(HandleCatalog):
    table_name = 'BioMaterial'
    model = Biomaterial


class MeasureUnitHandle(HandleCatalog):
    table_name = 'Unit'
    model = MeasureUnit
    table_columns = ('id', 'name', 'removed')
    params = table_columns


class AnalyzersHandle(HandleCatalog):
    table_name = 'Equipment'
    model = Analyzer
    table_columns = ('id', 'name', 'removed')
    params = table_columns


class ContainersHandle(HandleCatalog):
    table_name = 'ContainerType'
    model = Container
    table_columns = ('id', 'code', 'name', 'mnemonics', 'removed')
    params = ('id', 'code', 'name', 'short_name', 'removed')


class AnalysisGroupHandle(HandleCatalog):
    table_name = 'ServiceGroup'
    model = AnalysisGroup
    where = " where code not in ('178','186','178.5')"


class AnalysisHandle(HandleCatalog):
    table_name = ' ServiceGroup sg join Service se on se.service_group_id = sg.id join service_targets st on st.service_id = se.id join Target ta on ta.id = st.target_id '
    model = Analysis
    where = TARG_WHERE
    table_columns = ('ta.id', 'se.code', 'ta.name', 'sg.id', 'ta.description', 'ta.keywords', 'se.max_duration', 'ta.removed',)
    params = ('id', 'code', 'name', 'group_id', 'notes', 'tags', 'ready_for', 'removed')


class TestHandle(HandleCatalog):
    table_name = ' Test '
    model = Test
    where = TARG_WHERE
    table_columns = ('te.id', 'te.name', 'te.mnemonics', 'te.eng_name', 'te.unit_id')
    params = ('id', 'name', 'short_name', 'alternative_name', 'measuring_units_id')


class BiomaterialSamplingGroupHandle(HandleCatalog):
    table_name = 'BiomaterialSamplingGroup'
    model = BiomaterialSamplingGroup
    table_columns = ('id', 'name', 'biomaterial_id', 'container_type_id', 'removed')
    params = ('id', 'name', 'biomaterial_id', 'container_id', 'removed')


class AnalysisSamplingGroupHandle(HandleCatalog):
    simple_table_name = " ServiceGroup sg join Service se on se.service_group_id = sg.id join service_targets st on st.service_id = se.id join Target ta on ta.id = st.target_id join bsg_targets bsgt on bsgt.target_id = ta.id "
    profile_table_name = " Target t INNER JOIN TargetProfileNew tpn ON tpn.id = t.id INNER JOIN ProfileSample ps ON ps.profile_new_id = tpn.id INNER JOIN profilesample_targets pst ON pst.profile_sample_id = ps.id join (select bsgt.target_id as bsgt_target_id, bsg.id as bsg_id, bsg.biomaterial_id as bsg_bm_id from bsg_targets bsgt join BiomaterialSamplingGroup bsg on bsg.id = bsgt.sampling_group_id) as mb on mb.bsg_bm_id = ps.biomaterial_id and mb.bsgt_target_id = pst.simple_target_id join service_targets st on t.id = st.target_id join Service se on se.id = st.service_id join ServiceGroup sg on sg.id = se.service_group_id "
    model = AnalysisSamplingGroup
    simple_table_columns = ('bsgt.target_id', 'bsgt.sampling_group_id')
    profile_table_columns = ('t.id', 'mb.bsg_id')
    params = ('analysis_id', 'bsg_id')
    sql_simple = 'select distinct ' + ', '.join(simple_table_columns) + ' from ' + simple_table_name + ' ' + TARG_WHERE
    sql_profile = 'select distinct ' + ', '.join(profile_table_columns) + ' from ' + profile_table_name + ' ' + TARG_WHERE

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sql = self.sql_simple + ' union ' + self.sql_profile


class ProfileToSimpleHandle(HandleCatalog):
    table_name = ' Target t JOIN TargetProfileNew tpn ON tpn.id = t.id JOIN ProfileSample ps ON ps.profile_new_id = tpn.id JOIN profilesample_targets pst ON pst.profile_sample_id = ps.id JOIN Target tar ON tar.id = pst.simple_target_id join service_targets st on st.target_id = t.id join Service se on se.id = st.service_id join ServiceGroup sg on sg.id = se.service_group_id '
    model = ProfileToSimple
    table_columns = ('t.id', 'tar.id')
    where = TARG_WHERE
    params = ('profile_id', 'simple_target_id')


class AnalysisAnalyzersHandle(HandleCatalog):
    model = AnalysisAnalyzers
    table_name = ' ServiceGroup sg join Service se on se.service_group_id = sg.id join service_targets st on st.service_id = se.id join Target ta on ta.id = st.target_id join target_tests tt on tt.target_id = ta.id join Test te on tt.test_id = te.id join EquipmentTestMapping etm on etm.test_id = te.id '
    where = TARG_WHERE
    table_columns = ('ta.id', 'etm.equipment_id')
    params = ('analysis_id', 'analyzer_id')


class AnalysisTestsHandle(HandleCatalog):
    model = AnalysisTests
    table_name = 'target_tests'
    where = TARG_WHERE
    table_columns = ('target_id', 'test_id')
    params = ('analysis_id', 'test_id')



class Command(BaseCommand):

    def handle(self, *args, **options):
        mu = MeasureUnitHandle()
        alz = AnalyzersHandle()
        bm = BiomaterialsHandle()
        ct = ContainersHandle()
        ag = AnalysisGroupHandle()
        ana = AnalysisHandle()
        bsg = BiomaterialSamplingGroupHandle()
        catalog = (mu, alz, bm, ct, ana, bsg)
        ag.run()
        for element in catalog:
            element.start()
        for element in catalog:
            element.join()
        te = TestHandle()
        te.run()
        te.join()
        asg = AnalysisSamplingGroupHandle()
        pts = ProfileToSimpleHandle()
        aa = AnalysisAnalyzersHandle()
        tt = AnalysisTests()
        connections = (asg, pts, aa, tt)
        for connection in connections:
            connection.run_with_cleaning()
