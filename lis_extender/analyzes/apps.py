from django.apps import AppConfig


class AnalysesConfig(AppConfig):
    name = 'analyzes'

    def ready(self):
        import analyzes.signals
