from django.contrib import admin

# Register your models here.
from .models import TimeUnits, MeasureUnit

admin.site.register(TimeUnits)
admin.site.register(MeasureUnit)
