import datetime

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q, Count
from django.shortcuts import redirect
from django.views.generic import DetailView, FormView, UpdateView, ListView

from finance.calculations import color_generator
from mainapp.excel_operations import DoubleCodeError
from mainapp.views import AjaxDeleteView
from mainapp.views import ChangeLogMixin, FilteredListView
from .calculations import AnalyzesReport
from .engine import AnalyzesMappingUpdater
from .filters import AnalyzesFilter, AnalysisMappingFilter
from .forms import AnalysisEditForm, BlankEditForm, AnalysisMappingEditForm, MappingUploaderForm, \
    TestEditForm, AnalysisReportForm
from .models import Analysis, Analyzer, Biomaterial, Container, Blank, AnalysisLog, AnalysisMapping, AnalysisMappingLog, \
    Test


class AnalyzesListView(PermissionRequiredMixin, FilteredListView):
    queryset = Analysis.objects.all()
    template_name = 'analyzes/analyzes_list.html'
    filterset_class = AnalyzesFilter
    permission_required = 'analyzes.view_analysis'
    paginate_by = 25


class AnalyzesEditView(PermissionRequiredMixin, UpdateView, ChangeLogMixin):
    permission_required = 'analyzes.change_analysis'
    model = Analysis
    queryset = Analysis.objects.all()
    form_class = AnalysisEditForm
    template_name = 'analyzes/analysis_edit.html'
    log_connector_class = AnalysisLog

    def form_valid(self, form):
        orig_object = Analysis.objects.get(id=self.object.id)
        if orig_object == self.object:
            messages.info(self.request, 'Изменений нет')
            return redirect(self.object)
        else:
            self.make_log(attribs=form.cleaned_data.keys(), original_object=orig_object)
            super().form_valid(form)
            return redirect(self.object)


class AnalysisDetailView(DetailView):
    model = Analysis

class AnalysisHistoryView(PermissionRequiredMixin, DetailView):
    permission_required = 'analyzes.view_analysislog'
    model = Analysis
    template_name = 'analyzes/analysis_history.html'

class TestEditView(PermissionRequiredMixin, UpdateView):
    permission_required = 'analyzes.change_test'
    model = Test
    queryset = Test.objects.all()
    form_class = TestEditForm
    template_name = 'analyzes/test_edit.html'
    success_url = '/'

    def get_success_url(self):
        return self.request.GET.get('next')


class BiomaterialsListView(PermissionRequiredMixin, ListView):
    queryset = Biomaterial.objects.all()
    template_name = 'analyzes/base_shortlist.html'
    permission_required = 'analyzes.view_biomaterial'


class BiomaterialDetailView(PermissionRequiredMixin, DetailView):
    model = Biomaterial
    template_name = 'analyzes/biomaterial_detail.html'
    permission_required = 'analyzes.view_biomaterial'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bsg'] = self.object.biomaterialsamplinggroup_set.all()
        return context


class ContainersListView(PermissionRequiredMixin, ListView):
    queryset = Container.objects.all()
    template_name = 'analyzes/base_shortlist.html'
    permission_required = 'analyzes.view_container'


class ContainerDetailView(PermissionRequiredMixin, DetailView):
    model = Container
    template_name = 'analyzes/container_detail.html'
    permission_required = 'analyzes.view_container'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bsg'] = self.object.biomaterialsamplinggroup_set.all()
        return context


class AnalyzersListView(PermissionRequiredMixin, ListView):
    queryset = Analyzer.objects.all()
    template_name = 'analyzes/base_shortlist.html'
    permission_required = 'analyzes.view_analyzer'


class AnalyzerDetailView(PermissionRequiredMixin, DetailView):
    model = Analyzer
    template_name = 'analyzes/analyzer_detail.html'
    permission_required = 'analyzes.view_analyzer'


class BlanksListView(PermissionRequiredMixin, ListView):
    queryset = Blank.objects.all()
    template_name = 'analyzes/blanks_list.html'
    permission_required = 'analyzes.view_blank'


class BlankDetailView(PermissionRequiredMixin, DetailView):
    model = Blank
    template_name = 'analyzes/blank_detail.html'
    permission_required = 'analyzes.view_blank'


class BlankDeleteView(PermissionRequiredMixin, AjaxDeleteView):
    queryset = Blank.objects.all()
    permission_required = 'analyzes.delete_blank'


class BlankUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'analyzes.change_blank'
    model = Blank
    form_class = BlankEditForm
    template_name = 'analyzes/base_edit_form.html'
    queryset = Blank.objects.all()
    success_url = '/catalog/blanks/'


class BlankCreateView(PermissionRequiredMixin, FormView):
    form_class = BlankEditForm
    template_name = 'analyzes/base_create_form.html'
    permission_required = 'analyzes.add_blank'


class AnalysisMappingListView(PermissionRequiredMixin, FilteredListView):
    permission_required = 'analyzes.view_analysismapping'
    queryset = AnalysisMapping.objects.all()
    filterset_class = AnalysisMappingFilter
    paginate_by = 25


class AnalysisMappingEditView(PermissionRequiredMixin, UpdateView, ChangeLogMixin):
    permission_required = 'analyzes.change_analysismapping'
    template_name = 'analyzes/base_edit_form.html'
    queryset = AnalysisMapping.objects.all()
    model = AnalysisMapping
    form_class = AnalysisMappingEditForm
    log_connector_class = AnalysisMappingLog

    def form_valid(self, form):
        orig_object = AnalysisMapping.objects.get(id=self.object.id)
        if orig_object == self.object:
            messages.info(self.request, 'Изменений нет')
            return redirect('Mapping_list')
        else:
            self.make_log(attribs=form.cleaned_data.keys(), original_object=orig_object)
            super().form_valid(form)
            return redirect('Mapping_list')


class AnalysisMappingHistoryView(PermissionRequiredMixin, DetailView):
    permission_required = 'analyzes.change_analysismapping'
    model = AnalysisMapping


class AnalysisMappingUpload(PermissionRequiredMixin, FormView):
    form_class = MappingUploaderForm
    permission_required = 'analyzes.change_analysismapping'
    template_name = 'analyzes/analysismapping_upload.html'

    def form_valid(self, form):
        cleaned_form = form.cleaned_data
        file = cleaned_form.get('excel_file')
        lab = cleaned_form.get('lab')
        rewrite_existing = cleaned_form.get('rewrite')
        lq_code = cleaned_form.get('lq_service_code')
        ex_code = cleaned_form.get('ext_service_code')
        try:
            AnalyzesMappingUpdater(excel_file=file, lab=lab,
                                 rewrite_existing=rewrite_existing,
                                 lq_code=lq_code, ex_code=ex_code)
            messages.success(self.request, 'Загрузка успешно завершена!')
        except DoubleCodeError as dce:
            messages.error(self.request, f'Были обнаружены дублирующиеся записи для услуг: {dce}')
        except TypeError:
            messages.error(self.request, f'Код услуги не найден в файле')
        finally:
            return self.render_to_response(self.get_context_data(form=form))


class Top100Analyzes(PermissionRequiredMixin, ListView):
    template_name = 'analyzes/top100.html'
    permission_required = 'analyzes.view_analysis'
    queryset = Analysis.objects.annotate(sales_count=Count('sales', filter=Q(
        sales__creation_date__gt=datetime.datetime.now().date() - datetime.timedelta(weeks=4)))).exclude(
        sales_count=0).order_by('-sales_count')[:100]


class AnalyzesReportView(PermissionRequiredMixin, FormView):
    template_name = 'analyzes/analyzes_report.html'
    permission_required = 'analyzes.view_analysis'
    form_class = AnalysisReportForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        analyzes = cleaned_data.get('analyzes')
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        grouping = cleaned_data.get('grouping')
        response_as = cleaned_data.get('response_as')
        reports_to_response = {}
        reports_list = []
        for analysis in analyzes:
            try:
                report = AnalyzesReport(analysis=analysis, start_date=start_date, final_date=end_date,
                                        group_by=grouping)
                report.group_sales()
                reports_list.append({'analysis': analysis, 'color': color_generator(),
                                     'report': report.grouped_sales})
            except ValueError:
                messages.error(self.request, 'Дата начала должна быть меньше даты окончания!')
                return self.render_to_response(self.get_context_data(form=form))
        reports_to_response['reports'] = reports_list
        if response_as == 'tables':
            reports_to_response['tables'] = True
        if grouping == 'day':
            reports_to_response['by_day'] = True
        return self.render_to_response(self.get_context_data(form=form, **reports_to_response))
