from django import forms
from django.conf import settings

from mainapp.models import LaboratoryInfo
from .models import Analysis, Blank, TimeUnits, AnalysisMapping, Biomaterial, Test

DATE_INPUT_FORMATS = settings.DATE_INPUT_FORMATS


class BlankEditForm(forms.ModelForm):
    code = forms.CharField(required=True, label='Код бланка', widget=forms.TextInput(attrs={
        'class': 'uk-input', 'autocomplete': 'off'
    }))
    name = forms.CharField(required=True, label='Название', widget=forms.TextInput(attrs={
        'class': 'uk-input', 'autocomplete': 'off'
    }))
    blank = forms.FileField(required=False, label='Бланк в формате pdf', widget=forms.FileInput(attrs={
        'class': 'form-control-file'
    }))
    analyzes = forms.ModelMultipleChoiceField(required=False, label='Исследования', queryset=Analysis.objects.all(),
                                   widget=forms.SelectMultiple(attrs={
                                       'class': 'js-select-search',

                                   }))

    class Meta:
        model = Blank
        fields = [
            'code',
            'name',
            'blank',
            'analyzes',
        ]


class AnalysisEditForm(forms.ModelForm):
    code = forms.CharField(label='Код исследования',
                           widget=forms.TextInput(attrs={
                               'class': 'uk-input',
                               'readonly': 'true',
                           }))
    name = forms.CharField(label='Название',
                           widget=forms.TextInput(attrs={
                               'class': 'uk-input',
                               'readonly': 'true',
                           }))
    link = forms.URLField(required=False, label='Ссылка на сайте',
                          widget=forms.URLInput(attrs={'class': 'uk-input'}))
    take_instructions = forms.CharField(required=False, label='Инструкция по взятию биоматериала',
                                        widget=forms.Textarea(attrs={
                                            'class': 'uk-input',
                                            'rows': '10'
                                        }))
    store_conditions = forms.CharField(required=False, label='Условия хранения и транспортировки биоматериала',
                                       widget=forms.Textarea(attrs={
                                           'class': 'uk-input',
                                           'rows': '5'
                                       }))
    extra_appointment_days = forms.IntegerField(required=False, label='Сроки доназначения',
                                                widget=forms.NumberInput(attrs={
                                                    'class': 'uk-input'
                                                }))
    extra_appointment_units = forms.ModelChoiceField(required=False, label='', queryset=TimeUnits.objects.all(),
                                                     widget=forms.Select(attrs={
                                                         'class': 'js-select',

                                                     }))
    stability_8 = forms.IntegerField(required=False, label='Стабильность при температуре +2+8',
                                     widget=forms.NumberInput(attrs={
                                         'class': 'uk-input'
                                     }))
    stability_8_unit = forms.ModelChoiceField(required=False, label='Единицы измерения времени',
                                              queryset=TimeUnits.objects.all(),
                                              widget=forms.Select(attrs={
                                                  'class': 'js-select',

                                              }))
    stability_20 = forms.IntegerField(required=False, label='Стабильность при температуре -20',
                                      widget=forms.NumberInput(attrs={
                                          'class': 'uk-input'
                                      }))
    stability_20_unit = forms.ModelChoiceField(required=False, label='Единицы измерения времени',
                                               queryset=TimeUnits.objects.all(),
                                               widget=forms.Select(attrs={
                                                   'class': 'js-select',

                                               }))
    analysis_method = forms.CharField(required=False, label='Метод исследования',
                                      widget=forms.Textarea(attrs={
                                          'class': 'uk-input',
                                          'rows': '5'
                                      }))
    reagent = forms.FileField(required=False, label='Инструкция к реагенту',
                              widget=forms.FileInput(attrs={
                                  'class': 'form-control-file'
                              }))
    results_example = forms.FileField(required=False, label='Результат в формате pdf',
                                      widget=forms.FileInput(attrs={
                                          'class': 'form-control-file'
                                      }))
    clinical_meaning = forms.CharField(required=False, label='Клиническое значение/алгоритм обследования',
                                       widget=forms.Textarea(attrs={
                                           'class': 'uk-input',
                                           'rows': '10'
                                       }))
    influental_factors = forms.CharField(required=False, label='Влияние различных факторов на результат',
                                         widget=forms.Textarea(attrs={
                                             'class': 'uk-input',
                                             'rows': '10'
                                         }))
    get_ready = forms.CharField(required=False, label='Подготовка к исследованию',
                                widget=forms.Textarea(attrs={
                                    'class': 'uk-input',
                                    'rows': '10'
                                }))
    additional_biomaterials = forms.ModelMultipleChoiceField(required=False, label='Дополнительные биоматериалы',
                                              queryset=Biomaterial.objects.all(), widget=forms.SelectMultiple(
            attrs={'class': 'js-select-search', }))

    class Meta:
        model = Analysis
        fields = [
            'code',
            'name',
            'link',
            'take_instructions',
            'store_conditions',
            'extra_appointment_days',
            'extra_appointment_units',
            'stability_8',
            'stability_8_unit',
            'stability_20',
            'stability_20_unit',
            'analysis_method',
            'reagent',
            'results_example',
            'clinical_meaning',
            'influental_factors',
            'get_ready',
            'additional_biomaterials',
        ]


class TestEditForm(forms.ModelForm):
    references = forms.CharField(required=False, label='Референтные пределы',
                                 widget=forms.Textarea(attrs={
                                     'class': 'uk-input',
                                     'rows': '5'
                                 }))
    is_technical = forms.BooleanField(label='Не отображать в справочниках', required=False,
                                      widget=forms.CheckboxInput())

    class Meta:
        model = Test
        fields = [
            'references',
            'is_technical',
        ]


class AnalysisMappingEditForm(forms.ModelForm):
    cmd_code = forms.CharField(label='Код CMD', max_length=20, required=False,
                               widget=forms.TextInput(attrs={
                                   'class': 'uk-input', 'autocomplete': 'off'
                               }))
    helix_code = forms.CharField(label='Код Helix', max_length=20, required=False,
                                 widget=forms.TextInput(attrs={
                                     'class': 'uk-input', 'autocomplete': 'off'
                                 }))
    invitro_code = forms.CharField(label='Код InVitro', max_length=20, required=False,
                                   widget=forms.TextInput(attrs={
                                       'class': 'uk-input', 'autocomplete': 'off'
                                   }))
    kdl_code = forms.CharField(label='Код КДЛ', max_length=20, required=False,
                               widget=forms.TextInput(attrs={
                                   'class': 'uk-input', 'autocomplete': 'off'
                               }))
    gemotest_code = forms.CharField(label='Код Гемотест', max_length=20, required=False,
                                    widget=forms.TextInput(attrs={
                                        'class': 'uk-input', 'autocomplete': 'off'
                                    }))
    dialab_code = forms.CharField(label='Код Диалаб', max_length=20, required=False,
                                  widget=forms.TextInput(attrs={
                                      'class': 'uk-input', 'autocomplete': 'off'
                                  }))
    citilab_code = forms.CharField(label='Код Citilab', max_length=20, required=False,
                                   widget=forms.TextInput(attrs={
                                       'class': 'uk-input', 'autocomplete': 'off'
                                   }))
    efis_code = forms.CharField(label='Код Эфис', max_length=20, required=False,
                                widget=forms.TextInput(attrs={
                                    'class': 'uk-input', 'autocomplete': 'off'
                                }))
    unimed_code = forms.CharField(label='Код Юнимед', max_length=20, required=False,
                                  widget=forms.TextInput(attrs={
                                      'class': 'uk-input', 'autocomplete': 'off'
                                  }))

    class Meta:
        model = AnalysisMapping
        fields = [
            'cmd_code',
            'helix_code',
            'invitro_code',
            'kdl_code',
            'gemotest_code',
            'dialab_code',
            'citilab_code',
            'efis_code',
            'unimed_code',
        ]


def make_lab_choices():
    if LaboratoryInfo.objects.all().count() == 0:
        LaboratoryInfo.objects.create(name='Example lab', short_name='example')
    current_lab = LaboratoryInfo.objects.get(pk=1).short_name
    choices = []
    attrib_codes = [attrib for attrib in dir(AnalysisMapping) if
                    not callable(attrib) and 'code' in attrib and not attrib.startswith('_')]
    for attrib in attrib_codes:
        field_object = AnalysisMapping._meta.get_field(attrib)
        verbose_name = field_object.verbose_name
        if verbose_name != current_lab:
            choices.append((attrib, verbose_name))
    return choices


class MappingUploaderForm(forms.Form):
    excel_file = forms.FileField(required=True, label='Excel файл с сопоставлением')
    lq_service_code = forms.CharField(max_length=20, required=True, initial='K001',
                                      label='Код услуги, по которому\nбудет определен столбец с кодами услуг\nвашей лаборатории',
                                      widget=forms.TextInput(attrs={'class': 'uk-input', 'placeholder': 'K001'
                                                                    }))
    ext_service_code = forms.CharField(max_length=20, required=True,
                                       label='Код услуги, по которому\nбудет определен столбец с кодами услуг\nдругой лаборатории',
                                       widget=forms.TextInput(attrs={'class': 'uk-input', 'autocomplete': 'off',
                                                                     }))
    rewrite = forms.BooleanField(label='Перезаписать существующие коды', required=False,
                                 widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox',
                                                                   }))
    lab = forms.ChoiceField(label='Лаборатория',
                            widget=forms.Select(attrs={'class': 'uk-select'}),
                            choices=(make_lab_choices()), )

    def clean(self):
        cleaned_data = super().clean()
        excel_file = cleaned_data.get('excel_file')
        format_error = 'Требуется файл формата Excel 2010 и старше (xlsx)'
        if not excel_file.name.endswith('xlsx'):
            self.add_error('excel_file', format_error)
        elif excel_file.content_type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            self.add_error('excel_file', format_error)


class AddBiomaterialForm(forms.Form):
    biomaterial = forms.ModelChoiceField(required='True', label="Биоматериал",
                                         queryset=Biomaterial.objects.all(), widget=forms.Select(
            attrs={'class': 'js-select', }))


class AnalysisReportForm(forms.Form):
    analyzes = forms.ModelMultipleChoiceField(required=True, label='Услуги',
                                              queryset=Analysis.objects.all(), widget=forms.SelectMultiple(
            attrs={'class': 'js-select', }))
    start_date = forms.DateField(required=True, label='Начало периода',
                                 input_formats=DATE_INPUT_FORMATS,
                                 widget=forms.DateInput(attrs={
                                     'class': 'datepicker-here uk-input',
                                     'autocomplete': 'off',
                                 }))
    end_date = forms.DateField(required=True, label='Конец периода',
                               input_formats=DATE_INPUT_FORMATS,
                               widget=forms.DateInput(attrs={
                                   'class': 'datepicker-here uk-input',
                                   'autocomplete': 'off',
                               }))
    grouping = forms.ChoiceField(required=False, label='Группировка по',
                                 widget=forms.Select(attrs={'class': 'js-select'}),
                                 choices=(
                                     ('', 'Без группировки'),
                                     ('day', 'Дню'),
                                     ('week', 'Неделе'),
                                     ('month', 'Месяцу'),
                                 ), )
    response_as = forms.ChoiceField(required=False, label='Отчет в виде',
                                    widget=forms.Select(attrs={'class': 'js-select'}),
                                    choices=(
                                        ('tables', 'Таблиц'),
                                        ('graphs', 'Графиков'),
                                    ), )
