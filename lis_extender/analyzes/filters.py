import django_filters
from .models import Analysis, Analyzer, Biomaterial, Container, AnalysisGroup, Blank, AnalysisMapping, Test
from django import forms


class AnalyzesFilter(django_filters.FilterSet):
    group = django_filters.ModelMultipleChoiceFilter(field_name='group', label='Группы',
                                                     queryset=AnalysisGroup.objects.all(),
                                                     widget=forms.SelectMultiple(
                                                         attrs={'class': 'js-select-search',
                                                                'placeholder': 'Выберите группы',
                                                                }
                                                     ))
    blank = django_filters.ModelChoiceFilter(field_name='blank', label='Бланки',
                                             queryset=Blank.objects.all(),
                                             widget=forms.Select(
                                                 attrs={'class': 'js-select'}
                                             ))
    summary = django_filters.CharFilter(field_name='summary', label='Ключевые слова', lookup_expr='icontains',
                                        widget=forms.TextInput(
                                            attrs={'autocomplete': 'off',
                                                   'class': 'uk-input'}
                                        ))

    class Meta:
        model = Analysis
        fields = [
            'group',
            'blank',
            'summary',
        ]


class ContainerFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', label='Название', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = Container
        fields = [
            'name',
        ]


class BiomaterialFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', label='Название', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = Biomaterial
        fields = [
            'name',
        ]


class AnalyzerFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', label='Название', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = Analyzer
        fields = [
            'name',
        ]


class AnalysisMappingFilter(django_filters.FilterSet):
    summary = django_filters.CharFilter(field_name='summary', label='Ключевые слова', lookup_expr='icontains',
                                        widget=forms.TextInput(
                                            attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                        ))

    class Meta:
        model = AnalysisMapping
        fields = (
            'summary',
        )


class BlankFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(field_name='code', label='Код', lookup_expr='icontains', widget=forms.TextInput(
        attrs={'autocomplete': 'off', 'class': 'uk-input'}
    ))
    name = django_filters.CharFilter(field_name='name', label='Название', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = Container
        fields = [
            'code',
            'name',
        ]


class TestFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', label='Название', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = Test
        fields = [
            'name',
        ]
