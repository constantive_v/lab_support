from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from analyzes.models import Analysis, AnalysisMapping


@receiver(post_save, sender=Analysis)
def create_mapping(sender, instance, created, *args, **kwargs):
    if created:
        AnalysisMapping.objects.create(analysis=instance, summary=instance.summary)


@receiver(post_save, sender=Analysis)
def save_mapping(sender, instance, *args, **kwargs):
    if AnalysisMapping.objects.filter(analysis=instance).exists():
        instance.analysismapping.save()
    else:
        AnalysisMapping.objects.create(analysis=instance, summary=instance.summary)


@receiver(pre_save, sender=Analysis)
def save_summary(sender, instance, *args, **kwargs):
    instance.summary = f'{instance.code} {instance.name} {instance.tags}'


@receiver(pre_save, sender=AnalysisMapping)
def save_mapping_summary(sender, instance, *args, **kwargs):
    if AnalysisMapping.objects.filter(pk=instance.pk).exists():
        attrib_codes = [attrib for attrib in dir(instance) if
                        not callable(attrib) and 'code' in attrib and not attrib.startswith('_')]
        conc_string = ''
        for attrib in attrib_codes:
            conc_string += ' ' + str(getattr(instance, attrib))
        instance.summary = f'{instance.analysis.summary} {conc_string}'
