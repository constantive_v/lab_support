from mainapp.excel_operations import ExcelConverter
from .models import Analysis

class AnalyzesMappingUpdater(ExcelConverter):

    def __init__(self, excel_file, lab, rewrite_exising, lq_code, ex_code):
        super().__init__(excel_file)
        self.lab = lab
        self.rewrite_existing = rewrite_exising
        self.lq_code = lq_code
        self.ex_code = ex_code
        self.update_mapping()

    def update_mapping(self):
        lq_code_index = None
        ex_code_index = None
        codes = []
        for row in self._input_list:
            try:
                lq_code_index = row.index(self.lq_code)
            except ValueError:
                pass
            try:
                ex_code_index = row.index(self.ex_code)
            except ValueError:
                pass
            if ex_code_index and lq_code_index:
                break
        for row in self._input_list:
            lq_row_code = str(row[lq_code_index])
            if len(lq_row_code) > 3:
                codes.append(lq_row_code)
        self._check_list_for_uniqueness(codes)
        for row in self._input_list:
            lq_row_code = str(row[lq_code_index])
            ex_row_code = str(row[ex_code_index])
            if len(lq_row_code) > 3:
                try:
                    analysis = Analysis.objects.get(code=lq_row_code)
                    mapping_object = analysis.analysismapping
                    if self.rewrite_existing:
                        setattr(mapping_object, self.lab, ex_row_code)
                        mapping_object.save()
                    else:
                        code = getattr(mapping_object, self.ex_code)
                        if code in ('', ' ', None, '-'):
                            setattr(mapping_object, self.lab, ex_row_code)
                            mapping_object.save()
                except Analysis.DoesNotExist:
                    pass
