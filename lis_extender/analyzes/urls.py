from django.urls import path
from .views import AnalyzersListView, AnalyzerDetailView, AnalysisDetailView, AnalyzesListView, BlankDeleteView, \
    BlankDetailView, BlanksListView, BiomaterialDetailView, BiomaterialsListView, ContainerDetailView, \
    ContainersListView, BlankCreateView, BlankUpdateView, AnalyzesEditView, AnalysisMappingListView, \
    AnalysisMappingEditView, AnalysisMappingHistoryView, AnalysisMappingUpload, \
    TestEditView, Top100Analyzes, AnalyzesReportView, AnalysisHistoryView

urlpatterns = [
    path('analyzers/', AnalyzersListView.as_view(), name='Analyzers list'),
    path('analyzers/<int:pk>/', AnalyzerDetailView.as_view(), name='Analyzer'),
    path('top100/', Top100Analyzes.as_view(), name='Top100'),
    path('biomaterials/', BiomaterialsListView.as_view(), name='Biomaterials list'),
    path('biomaterials/<int:pk>/', BiomaterialDetailView.as_view(), name='Biomaterial'),
    path('containers/', ContainersListView.as_view(), name='Container list'),
    path('containers/<int:pk>/', ContainerDetailView.as_view(), name='Container'),
    path('analyzes/', AnalyzesListView.as_view(), name='Analyzes list'),
    path('report/', AnalyzesReportView.as_view(), name='Analyzes report'),
    path('analyzes/<int:pk>/', AnalysisDetailView.as_view(), name='Analysis'),
    path('analyzes/<int:pk>/edit/', AnalyzesEditView.as_view(), name='Analysis edit'),
    path('analyzes/<int:pk>/history/', AnalysisHistoryView.as_view(), name='Analysis history'),
    path('tests/<int:pk>/edit/', TestEditView.as_view(), name='Test edit'),
    path('blanks/', BlanksListView.as_view(), name='Blanks list'),
    path('blanks/add/', BlankCreateView.as_view(), name='Create blank'),
    path('blanks/<slug>/', BlankDetailView.as_view(), name='Blank'),
    path('blanks/<slug>/edit/', BlankUpdateView.as_view(), name='Edit blank'),
    path('blanks/<slug>/delete/', BlankDeleteView.as_view(), name='Delete blank'),
    path('mapping/', AnalysisMappingListView.as_view(), name='Mapping_list'),
    path('mapping/upload/', AnalysisMappingUpload.as_view(), name='Upload_mapping'),
    path('mapping/<int:pk>/', AnalysisMappingHistoryView.as_view(), name='Mapping'),
    path('mapping/<int:pk>/edit/', AnalysisMappingEditView.as_view(), name='Mapping edit'),
]
