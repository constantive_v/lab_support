from io import BytesIO

from django.contrib import messages
from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import redirect, reverse
from django.views.generic import DetailView, FormView, UpdateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from reportlab.graphics.barcode import code128
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas

from .filters import BagLpuFilter, LpuFilter, ScannedBagsFilter
from .forms import ScannedBagForm, BagLpuForm, BagLpuAdd, BagsReportForm, BagLpuEdit
from .models import ScannedBag, LPU, BagLPU, BagLPULog
from mainapp.views import ChangeLogMixin, FilteredListView, AjaxDeleteView


class ScanBagView(PermissionRequiredMixin, FormView):
    form_class = ScannedBagForm
    template_name = 'bags/scan_form.html'
    permission_required = 'bags.add_scannedbag'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['object_list'] = ScannedBag.objects.all()[:10]
        return context

    def form_valid(self, form):
        barcode = form.cleaned_data.get('barcode')
        lpu = BagLPU.objects.get(barcode=barcode)
        ScannedBag.objects.create(barcode=barcode, lpu=lpu, creator=self.request.user)
        return redirect(reverse('scan_bag'))


class ScanBagDelete(PermissionRequiredMixin, AjaxDeleteView):
    model = ScannedBag
    permission_required = 'bags.delete_scannedbag'


class LpuDetail(PermissionRequiredMixin, DetailView):
    model = LPU
    permission_required = 'bags.view_lpu'


class BagLPUDetail(PermissionRequiredMixin, DetailView):
    template_name = 'bags/bag_lpu_detail.html'
    model = BagLPU
    permission_required = 'bags.view_baglpu'


class BagLPUUpdate(PermissionRequiredMixin, UpdateView, ChangeLogMixin):
    model = BagLPU
    form_class = BagLpuEdit
    template_name = "bags/bag_lpu_update.html"
    queryset = BagLPU.objects.all()
    permission_required = 'bags.change_baglpu'
    log_connector_class = BagLPULog

    def form_valid(self, form):
        form_data = form.cleaned_data
        barcode = form_data.get('barcode')
        lpu = form_data.get('lpu')
        bag_lpu = self.object
        orig_object = BagLPU.objects.get(pk=self.kwargs.get('pk'))
        if orig_object.barcode == barcode and orig_object.lpu == lpu:
            messages.info(self.request, 'Изменений нет')
            return redirect(bag_lpu)
        else:
            self.make_log(attribs=form_data.keys(), original_object=orig_object)
            messages.success(self.request, 'Данные по контейнеру обновлены.')
            super().form_valid(form)
            return redirect(bag_lpu)


class BagLPUCreate(PermissionRequiredMixin, FormView):
    form_class = BagLpuForm
    success_url = "/bags/bag_lpu/"
    template_name = "bags/bag_lpu_add.html"
    lpu = ''
    permission_required = 'bags.add_baglpu'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        if pk:
            self.lpu = LPU.objects.get(pk=pk)
        context['lpu'] = self.lpu
        return context

    def form_valid(self, form, **kwargs):
        pk = self.kwargs.get('pk')
        barcodes = BagLPU.objects.all()
        if form.cleaned_data.get('autogenerate_barcode'):
            barcode = str(int(barcodes.aggregate(Max('barcode')).get('barcode__max')) + 1)
        else:
            barcode = form.cleaned_data['barcode']
        if pk:
            lpu = LPU.objects.get(pk=pk)
        else:
            lpu = form.cleaned_data['lpu']
        creator = self.request.user
        bag_lpu = BagLPU(barcode=barcode, lpu=lpu, creator=creator)
        bag_lpu.save()
        messages.success(self.request, f'Контейнер с штрих-кодом {barcode} успешно добавлен к ЛПУ {lpu}.')
        return redirect(bag_lpu)


class AddBagtoLPU(BagLPUCreate):
    form_class = BagLpuAdd
    template_name = 'bags/bag_lpu_add.html'


class BagsReportView(PermissionRequiredMixin, FormView):
    template_name = "bags/report_form.html"
    form_class = BagsReportForm
    permission_required = 'bags.view_permissionreport'

    def form_valid(self, form, **kwargs):
        context = super().get_context_data(**kwargs)
        result = []
        cleaned_data = form.cleaned_data
        lpus = cleaned_data['lpu']
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']
        for lpu in lpus:
            bags_lpus = BagLPU.objects.filter(lpu=lpu)
            if len(bags_lpus) > 0:
                bags_dict = {'lpu': str(lpu), 'data': []}
                for bags_lpu in bags_lpus:
                    filtered_bags = ScannedBag.objects.filter(lpu=bags_lpu, scan_date__range=(start_date, end_date))
                    detail = {}
                    detail['count'] = len(filtered_bags)
                    detail['scanned_bags'] = filtered_bags
                    bags_dict['data'].append({'barcode': bags_lpu.barcode, 'detail': detail})
                result.append(bags_dict)
        context['report'] = result
        context['start_date'] = start_date
        context['end_date'] = end_date
        return self.render_to_response(context)


class BagsLpuList(PermissionRequiredMixin, FilteredListView):
    queryset = BagLPU.objects.all()
    template_name = 'bags/bag_lpu_list.html'
    paginate_by = 25
    filterset_class = BagLpuFilter
    permission_required = 'bags.view_baglpu'


class LpuList(PermissionRequiredMixin, FilteredListView):
    queryset = LPU.objects.all()
    template_name = 'bags/lpu_list.html'
    paginate_by = 25
    filterset_class = LpuFilter
    permission_required = 'bags.view_lpu'


class ScannedBagList(PermissionRequiredMixin, FilteredListView):
    queryset = ScannedBag.objects.select_related('lpu__lpu').all()
    template_name = 'bags/scanned_list.html'
    paginate_by = 15
    filterset_class = ScannedBagsFilter
    permission_required = 'bags.view_scannedbag'


def generate_barcode_pdf(barcode, lpu_code):
    pdfmetrics.registerFont(TTFont('Vera', 'Vera.ttf'))
    pdf_file = BytesIO()
    pdf_file.seek(0)
    lpu_code = lpu_code
    c = canvas.Canvas(pdf_file, pagesize=(44 * mm, 25 * mm))
    barcode = code128.Code128Auto(barcode, barWidth=0.4 * mm, barHeight=12 * mm, humanReadable=1)
    barcode.drawOn(c, 1 * mm, 5 * mm)
    t = c.beginText()
    t.setFont('Vera', 6)
    t.setTextOrigin(1 * mm, 20 * mm)
    t.textLine(lpu_code)
    c.drawText(t)
    c.showPage()
    c.save()
    return pdf_file


def return_barcode(request, pk):
    if request.method == 'GET':
        BL = BagLPU.objects.get(pk=pk)
        barcode = BL.barcode
        lpu_code = BL.lpu.name
        buffer = generate_barcode_pdf(barcode, lpu_code)
        response = HttpResponse(buffer.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = f'filename=barcode.pdf'
        return response
