import pyodbc
from django.conf import settings
from bags.models import LPU, BagLPU, ScannedBag
import csv
from django.core.management.base import BaseCommand
from datetime import datetime
from lis_extender.decorators import log_exceptions

LIS_DB = settings.LIS_DB
LOG_PATH = settings.LOG_PATH


server = LIS_DB.get('server')
database = LIS_DB.get('database')
username = LIS_DB.get('username')
password = LIS_DB.get('password')
IMPORT_LPU_LOG = 'import_lpu_errors.log'


class Command(BaseCommand):

    @log_exceptions
    def handle(self, *args, **kwargs):
        output_list = []
        sql = "SELECT DISTINCT ho.id, ho.code, ho.name, ho.disable_requests FROM Hospital ho where ho.removed = 0 and ho.code not like '9999%'"
        cnxn = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
        cursor = cnxn.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()
        while row:
            mod_row = (int(row[0]), row[1], row[2], int(row[3]))
            output_list.append(mod_row)
            row = cursor.fetchone()
        for hospital in output_list:
            hospital_id = hospital[0]
            hospital_code = hospital[1]
            hospital_name = hospital[2]
            hospital_disabled = hospital[3]
            is_active = True if hospital_disabled == 0 else False
            if LPU.objects.filter(id=hospital_id).exists():
                lpu = LPU.objects.get(id=hospital_id)
                lpu.code = hospital_code
                lpu.name = hospital_name
                lpu.is_active = is_active
                lpu.save()
            else:
                lpu = LPU(id=hospital_id, code=hospital_code, name=hospital_name, is_active=is_active)
                lpu.save()


@log_exceptions
def import_bag_lpu(source_csv):
    lpus = LPU.objects.all()
    with open(source_csv, mode='r', encoding='utf8') as r:
        csvreader = csv.reader(r, delimiter=',', quotechar='"')
        output_list = [row for row in csvreader if row[0] != '']
    for row in output_list:
        if lpus.filter(code=row[1]).exists() and not BagLPU.objects.filter(barcode=row[0]).exists():
            proper_lpu = LPU.objects.get(code=row[1])
            lbag = BagLPU(barcode=row[0], lpu=proper_lpu)
            lbag.save()
        else:
            wr = open('errors', mode='a')
            wr.write(str(row) + '\n')


@log_exceptions
def barcodes(source_csv):
    with open(source_csv, mode='r', encoding='utf8') as r:
        csvreader = csv.reader(r, delimiter=',', quotechar='"')
        output_list = [row for row in csvreader]
    barcodes = [row[0] for row in output_list]
    return barcodes


@log_exceptions
def import_scanned(source_csv):
    with open(source_csv, mode='r', encoding='utf8') as r:
        csvreader = csv.reader(r, delimiter=',', quotechar='"')
        output_list = [row for row in csvreader if row[0] != '']
    for row in output_list:
        if len(row[0]) < 7:
            if BagLPU.objects.filter(barcode=row[0]).exists() and BagLPU.objects.filter(
                    lpu__in=LPU.objects.filter(code=row[1])):
                try:
                    date = datetime.strptime(row[4], '%d.%m.%Y %H:%M:%S')
                    sb = ScannedBag(barcode=row[0], lpu=BagLPU.objects.get(barcode=row[0]), scan_date=date)
                    sb.save()
                except:
                    pass


@log_exceptions
def remove_scanned():
    start_date = datetime.strptime('14.03.2019 14:50:35', '%d.%m.%Y %H:%M:%S')
    end_date = datetime.strptime('14.03.2019 14:50:45', '%d.%m.%Y %H:%M:%S')
    ScannedBag.objects.filter(scan_date__range=(start_date, end_date)).delete()
