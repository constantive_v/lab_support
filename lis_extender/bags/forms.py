from datetime import datetime, timedelta

from django.conf import settings
from django import forms
from .models import LPU, BagLPU

DATE_INPUT_FORMATS = settings.DATE_INPUT_FORMATS


class ScannedBagForm(forms.Form):
    barcode = forms.IntegerField(required=True, label='Штрих-код сумки',
                                 widget=forms.NumberInput(attrs={'class': 'uk-input',
                                                                 'autocomplete': 'off'}))

    def clean(self):
        form_data = self.cleaned_data
        barcode = form_data['barcode']
        try:
            bag = BagLPU.objects.get(barcode=barcode)
            hour_ago = datetime.now() - timedelta(hours=1)
            if bag.scannedbag_set.filter(scan_date__gt=hour_ago).exists():
                self.add_error('barcode', f'Контейнер {barcode} был сканирован менее часа назад!')
        except BagLPU.DoesNotExist:
            self.add_error('barcode', f'Контейнер {barcode} не привязан к ЛПУ!')


class BagLpuForm(forms.Form):
    autogenerate_barcode = forms.BooleanField(label='Сгенерировать код сумки', initial=True, required=False,
                                              widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox'}))
    barcode = forms.IntegerField(label="Штрих-код сумки", required=False,
                                 widget=forms.NumberInput(attrs={'class': 'uk-input',
                                                                 'autocomplete': 'off'}))
    lpu = forms.ModelChoiceField(required='True', label="ЛПУ",
                                 queryset=LPU.objects.all(), widget=forms.Select(
            attrs={'class': 'js-select-search'}))

    def clean(self):
        form_data = self.cleaned_data
        barcode = form_data['barcode']
        try:
            bag = BagLPU.objects.get(barcode=barcode)
            self.add_error('barcode', f'Контейнер с штрих-кодом {barcode} уже привязан к ЛПУ {bag.lpu}.')
        except BagLPU.DoesNotExist:
            pass


class BagLpuEdit(forms.ModelForm):
    barcode = forms.IntegerField(required=True, label="Штрих-код сумки",
                                 widget=forms.NumberInput(attrs={'class': 'uk-input',
                                                                 'autocomplete': 'off'}))
    lpu = forms.ModelChoiceField(required='True', label="ЛПУ",
                                 queryset=LPU.objects.all(), widget=forms.Select(
            attrs={'class': 'js-select-search'}))

    class Meta:
        model = BagLPU
        fields = [
            'barcode',
            'lpu'
        ]


class BagLpuAdd(forms.Form):
    autogenerate_barcode = forms.BooleanField(label='Сгенерировать код сумки', initial=True, required=False,
                                              widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox'}))
    barcode = forms.IntegerField(label="Штрих-код сумки", required=False,
                                 widget=forms.NumberInput(attrs={'class': 'uk-input',
                                                                 'autocomplete': 'off'}))


class BagsReportForm(forms.Form):
    lpu = forms.ModelMultipleChoiceField(required=True, label='ЛПУ',
                                         queryset=LPU.objects.all(), widget=forms.SelectMultiple(
            attrs={'class': 'js-select-search',
                   }))
    start_date = forms.DateField(required=True, label='Начало периода',
                                 input_formats=DATE_INPUT_FORMATS,
                                 widget=forms.DateInput(attrs={'class': 'uk-input datepicker-here',
                                                               }))
    end_date = forms.DateField(required=True, label='Конец периода',
                               input_formats=DATE_INPUT_FORMATS,
                               widget=forms.DateInput(attrs={'class': 'uk-input datepicker-here',
                                                             }))
