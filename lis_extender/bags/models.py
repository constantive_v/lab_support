from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from mainapp.models import LogEvent
from django.utils import timezone

DATETIME_INPUT_FORMATS = settings.DATETIME_INPUT_FORMATS


class LPU(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    code = models.CharField(max_length=20, unique=True)
    name = models.TextField()
    is_active = models.BooleanField()

    def __str__(self):
        return f'{self.code} {self.name}'

    def get_absolute_url(self):
        return reverse('lpu', kwargs={"pk": self.pk})

    def get_finance_url(self):
        return reverse('Customer details', kwargs={'pk': self.pk})

    def get_add_bag_url(self):
        return reverse('add_bag_to_lpu', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('code',)


class BagLPU(models.Model):
    barcode = models.IntegerField(unique=True)
    lpu = models.ForeignKey(to=LPU, on_delete=models.SET_NULL, blank=True, null=True, related_name='bag',
                            related_query_name='bags')
    create_date = models.DateTimeField(default=timezone.now)
    creator = models.ForeignKey(to=User, on_delete=models.SET_NULL, blank=True, null=True, default=None,
                                related_name='creator')

    def __str__(self):
        return f'ШК: {self.barcode} ЛПУ: {self.lpu}'

    def get_absolute_url(self):
        return reverse('bag_lpu', kwargs={'pk': self.pk})

    def get_edit_url(self):
        return reverse('bag_lpu_edit', kwargs={'pk': self.pk})

    def get_barcode_url(self):
        return reverse('bag_lpu_barcode', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('barcode',)


class ScannedBag(models.Model):
    barcode = models.IntegerField()
    scan_date = models.DateTimeField(default=timezone.now)
    lpu = models.ForeignKey(to=BagLPU, on_delete=models.CASCADE)
    incoming = models.BooleanField(default=True)
    creator = models.ForeignKey(to=User, on_delete=models.SET_NULL, blank=True, null=True, default=None)

    def __str__(self):
        formatted_scan_date = self.scan_date.strftime(DATETIME_INPUT_FORMATS[0])
        return f'{self.barcode} {formatted_scan_date} {self.lpu.lpu}'

    def get_absolute_url(self):
        return reverse('scanbag_delete', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('scanbag_delete', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('-scan_date',)


class PermissionReport(models.Model):
    name = models.CharField(max_length=2)


class BagLPULog(models.Model):
    obj_instance = models.ForeignKey(to=BagLPU, on_delete=models.CASCADE)
    event = models.ForeignKey(to=LogEvent, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-event__timestamp',)
