import django_filters
from django import forms
from django.conf import settings

from .models import LPU, BagLPU, ScannedBag

DATE_INPUT_FORMATS = settings.DATE_INPUT_FORMATS


class BagLpuFilter(django_filters.FilterSet):
    lpu = django_filters.ModelMultipleChoiceFilter(field_name='lpu', label='ЛПУ', lookup_expr='in',
                                                   queryset=LPU.objects.all(),
                                                   widget=forms.SelectMultiple(
                                                       attrs={
                                                           'class': 'js-select-search',
                                                       }
                                                   ))

    barcode = django_filters.CharFilter(field_name='barcode', label='Штрих-код',
                                        widget=forms.TextInput(
                                            attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                        ))

    class Meta:
        model = BagLPU
        fields = [
            'lpu',
            'barcode',
        ]


class LpuFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(field_name='code', label='Код ЛПУ', widget=forms.TextInput(
        attrs={'autocomplete': 'off', 'class': 'uk-input'}
    ))
    name = django_filters.CharFilter(field_name='name', label='Название ЛПУ', lookup_expr='icontains',
                                     widget=forms.TextInput(
                                         attrs={'autocomplete': 'off', 'class': 'uk-input'}
                                     ))

    class Meta:
        model = LPU
        fields = ['code', 'name']


class ScannedBagsFilter(django_filters.FilterSet):
    lpu = django_filters.ModelMultipleChoiceFilter(field_name='lpu', label='Термоконтейнеры', lookup_expr='in',
                                                   queryset=BagLPU.objects.all(),
                                                   widget=forms.SelectMultiple(
                                                       attrs={
                                                           'class': 'js-select-search',
                                                       }
                                                   ))
    start_date = django_filters.DateFilter(field_name='scan_date', label='Начало сканирования', lookup_expr='gt',
                                           input_formats=DATE_INPUT_FORMATS,
                                           widget=forms.DateInput(attrs={
                                               'class': 'datepicker-here uk-input',
                                               'autocomplete': 'off',
                                           }))
    end_date = django_filters.DateFilter(field_name='scan_date', label='Окончание сканирования', lookup_expr='lt',
                                         input_formats=DATE_INPUT_FORMATS,
                                         widget=forms.DateInput(attrs={
                                             'class': 'datepicker-here uk-input',
                                             'autocomplete': 'off',
                                         }))

    class Meta:
        model = ScannedBag
        fields = [
            'lpu',
            # 'scan_date'
        ]
