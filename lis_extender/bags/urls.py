from django.urls import path
from .views import LpuDetail, BagLPUDetail, BagLPUCreate, ScanBagView, AddBagtoLPU, BagsReportView, BagsLpuList, \
    LpuList, ScannedBagList, return_barcode, BagLPUUpdate, ScanBagDelete

urlpatterns = [
    path('bag_reg/', ScannedBagList.as_view(), name='scanbag_list'),
    path('bag_reg/add/', ScanBagView.as_view(), name='scan_bag'),
    path('bag_reg/<int:pk>/delete/', ScanBagDelete.as_view(), name='scanbag_delete'),
    path('lpu/', LpuList.as_view(), name='lpu_bags_list'),
    path('lpu/<int:pk>/', LpuDetail.as_view(), name='lpu'),
    path('lpu/<int:pk>/add_barcode/', AddBagtoLPU.as_view(), name='add_bag_to_lpu'),
    path('bag_lpu/', BagsLpuList.as_view(), name='bag_lpu_list'),
    path('bag_lpu/<int:pk>/', BagLPUDetail.as_view(), name='bag_lpu'),
    path('bag_lpu/<int:pk>/barcode.pdf', return_barcode, name='bag_lpu_barcode'),
    path('bag_lpu/add/', BagLPUCreate.as_view(), name='add_baglpu'),
    path('bag_lpu/<int:pk>/edit/', BagLPUUpdate.as_view(), name='bag_lpu_edit'),
    path('report/', BagsReportView.as_view(), name='bags_report'),
]
