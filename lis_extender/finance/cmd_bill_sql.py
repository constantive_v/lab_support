import pyodbc
from django.conf import settings

LIS_DB = settings.LIS_DB
LOG_PATH = settings.LOG_PATH

server = LIS_DB.get('server')
database = LIS_DB.get('database')
username = LIS_DB.get('username')
password = LIS_DB.get('password')



def get_request_list(start_date, end_date):
    output_list = []
    sql = f"Select distinct re.internal_nr \
    , (isnull(pa.last_name, '') + ' ' + isnull(Pa.first_name, '') + ' ' + isnull(pa.middle_name, '')), \
     ExternalSystemMappingTarget.external_code from Request re join Patient pa on pa.id = re.patient_id \
    join Hospital ho on ho.id = re.hospital_id join Sample sa on re.id = sa.request_id join \
    Department de on de.id = sa.department_id join sample_targets st on st.sample_id = sa.id \
    left join sample_profile_targets spt on spt.sample_id = sa.id join Target ta on \
    st.target_id = ta.id or spt.target_id = ta.id join lims.ExternalSystemMappingTarget \
    on ExternalSystemMappingTarget.target_id = ta.id join lims.ExternalSystemMapping on \
    ExternalSystemMapping.id = ExternalSystemMappingTarget.mapping_id where \
    de.code = 'CMD' and cast(registration_date as datetime) between '{start_date}' and '{end_date}' \
    and sa.defect_state = '0' and Ho.code not like '999%' and ExternalSystemMapping.name = 'LQ' \
    and ExternalSystemMappingTarget.external_code not like '%/%'; "

    cnxn = pyodbc.connect(
        'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    cursor.execute(sql)
    row = cursor.fetchone()
    while row:
        row = list(row)
        output_list.append(row)
        row = cursor.fetchone()

    return output_list

def get_hospitals_list():
    output_list = []
    sql = 'SELECT DISTINCT ho.id, ho.code, ho.name, ho.removed FROM Hospital ho'
    cnxn = pyodbc.connect(
        'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
    cursor = cnxn.cursor()
    cursor.execute(sql)
    row = cursor.fetchone()
    while row:
        mod_row = (int(row[0]), row[1], row[2], int(row[3]))
        output_list.append(mod_row)
        row = cursor.fetchone()

    return output_list