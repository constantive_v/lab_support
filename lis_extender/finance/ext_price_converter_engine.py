from mainapp.excel_operations import ExcelConverter
from analyzes.models import AnalysisMapping


class ExternalPriceConverter(ExcelConverter):

    def __init__(self, excel_file, lab, ext_code):
        super().__init__(excel_file)
        self.lab = lab
        self.ext_code = ext_code
        self.run()

    def insert_lq_codes(self):
        ex_code_index = None
        for row in self._input_list:
            try:
                ex_code_index = row.index(self.ext_code)
            except ValueError:
                pass
            if ex_code_index is not None:
                break
        lq_code_index = ex_code_index + 1
        lq_name_index = lq_code_index + 1
        for num, row in enumerate(self._input_list):
            ext_row_code = row[ex_code_index]
            if ext_row_code:
                mapping_object = AnalysisMapping.objects.filter(**{self.lab: ext_row_code}).first()
                if mapping_object is not None:
                    lq_code = mapping_object.analysis.code
                    lq_name = mapping_object.analysis.name
                    self._input_list[num].insert(lq_code_index, lq_code)
                    self._input_list[num].insert(lq_name_index, lq_name)
                else:
                    self._input_list[num].insert(lq_code_index, 'НН')
                    self._input_list[num].insert(lq_name_index, 'Соответствие не найдено')

    def run(self):
        self.insert_lq_codes()
        self._list_to_excel(self._input_list)
