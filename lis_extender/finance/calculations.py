import datetime
from django.db.models import Sum
from .models import Sale
from random import randint


class FinancialReport:
    def __init__(self, start_date, final_date=None, group_by=None, lpu=None):
        self.obj = lpu
        self.grouped_sales = None
        if type(start_date) is datetime.date:
            self.start_date = start_date
        else:
            self.start_date = start_date.date()
        if type(final_date) is datetime.date:
            self.final_date = final_date
        elif final_date is None:
            self.final_date = datetime.datetime.now().date()
        else:
            self.final_date = final_date.date()
        self.group_by = group_by
        if self.obj is None:
            self.filtered_sales = Sale.objects.filter(creation_date__range=(start_date, final_date))
        else:
            self.filtered_sales = self.obj.lpu_sales.filter(creation_date__range=(start_date, final_date))

    def add_month(self, start_date):
        month = start_date.month
        year = start_date.year + month // 12
        month = month % 12 + 1
        # day = min(start_date.day, calendar.monthrange(year, month)[1])  # если надо брать по месяцу от даты, а не от первого числа
        day = 1
        return datetime.date(year, month, day)

    def add_week(self, start_date):
        monday_date = start_date - datetime.timedelta(days=start_date.weekday())
        sunday_date = monday_date + datetime.timedelta(days=7)
        return sunday_date

    def add_day(self, start_date):
        return start_date + datetime.timedelta(days=1)

    def add_nothing(self):
        return self.final_date

    def get_end_date(self, start_date):
        if self.group_by == 'week':
            end_date = self.add_week(start_date)
        elif self.group_by == 'month':
            end_date = self.add_month(start_date)
        elif self.group_by == 'day':
            end_date = self.add_day(start_date)
        else:
            end_date = self.add_nothing()
        return end_date

    def get_summ_sales(self, queryset):  # Оборот
        return queryset.aggregate(Sum('service_price'))['service_price__sum']

    def get_requests_count(self, queryset):  # Кол-во чеков
        return queryset.values_list('request_id').distinct().count()

    def get_avg_check(self, sales_summ, requests_count):  # Средний чек
        return int(sales_summ / requests_count)

    def get_avg_price(self, sales_summ, sales_count):  # Средняя стоимость покупки
        return int(sales_summ / sales_count)

    def get_check_deep(self, sales_count, requests_count):  # Глубина чека
        return int(sales_count / requests_count)


    def group_sales(self):
        sales_by_periods = []
        if self.start_date >= self.final_date and self.group_by != 'day':
            raise ValueError
        start_date = self.start_date
        while start_date < self.final_date:
            end_date = self.get_end_date(start_date=start_date)
            if end_date > self.final_date:
                end_date = self.final_date
            sales_in_period = self.filtered_sales.filter(creation_date__range=(start_date, end_date))
            count_sales = sales_in_period.count()
            if count_sales > 0:
                summ_sales = self.get_summ_sales(sales_in_period)  # Оборот
                requests_in_period = self.get_requests_count(sales_in_period)  # Кол-во чеков
                avg_check = self.get_avg_check(summ_sales, requests_in_period)  # Средний чек
                avg_price = self.get_avg_price(summ_sales, count_sales)  # Средняя стоимость покупки
                check_deep = self.get_check_deep(count_sales, requests_in_period)  # Глубина чека
            else:
                avg_price = 0
                requests_in_period = 0
                summ_sales = 0
                avg_check = 0
                check_deep = 0
            sales_by_periods.append({'start_date': start_date, 'end_date': end_date,
                                     'requests': requests_in_period, 'avg_price': avg_price,
                                     'avg_check': avg_check, 'turnover': summ_sales,
                                     'check_deep': check_deep})
            start_date = end_date
        self.grouped_sales = sales_by_periods


def color_generator():
    first = str(randint(30, 230))
    second = str(randint(30, 230))
    third = str(randint(30, 230))
    return f'rgb({first}, {second}, {third})'
