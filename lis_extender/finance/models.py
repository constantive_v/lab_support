from django.db import models
from bags.models import LPU
from analyzes.models import Analysis


# Create your models here.
class FinancePerm(models.Model):
    class Meta:
        permissions = (
            ('cmd_bill_compare', 'Can compare cmd bill'),
            ('price_convert', 'Can convert prices'),
            ('view_own_sales_report', 'Can view report'),
            ('view_analysis_sales', 'Can view top 100 best selling analysis'),
            ('view_own_lpu_sales', 'Can view related lpus')
        )


class Sale(models.Model):
    id = models.IntegerField(primary_key=True)
    creation_date = models.DateTimeField()
    request_id = models.IntegerField()
    analysis = models.ForeignKey(to=Analysis, on_delete=models.CASCADE, related_name='sales')
    service_price = models.DecimalField(max_digits=9, decimal_places=2)
    lpu = models.ForeignKey(to=LPU, on_delete=models.CASCADE, related_name='lpu_sales')
