# -*- coding: utf-8 -*-
from mainapp.excel_operations import ExcelConverter


class LISExcelCovnerter(ExcelConverter):

    def __init__(self, excel_file, pricelist_code, target_code, round_needed, contains_cito,
                 b2b_price):
        super().__init__(excel_file)
        self._pricelist_code = pricelist_code
        self._target_code = target_code
        self._round_needed = round_needed
        self._contains_cito = contains_cito
        self._b2b_price = True if b2b_price == 'B2B' else False
        self.header = [self._pricelist_code, '', '', '', 'z'] if self._b2b_price else ['', '', '', 'z']
        self.run()

    def _convert_list_for_upload(self):
        code_index = None
        for row in self._input_list:
            try:
                code_index = row.index(self._target_code)
                if code_index != None:
                    break
            except ValueError:
                pass
        list_to_return = list()
        codes = list()
        for row2 in self._input_list:
            code = row2[code_index]
            last = row2[-1]
            prelast = row2[-2]
            if len(code) > 3 and len(code) < 8:
                if self._contains_cito:
                    if self._round_needed:
                        try:
                            if last.endswith('.5'):
                                cito_price = int(round(float(last) + 0.5))
                            else:
                                cito_price = int(round(float(last)))
                            if prelast.endswith('.5'):
                                price = int(round(float(prelast) + 0.5))
                            else:
                                price = int(round(float(prelast)))
                        except ValueError:
                            price = prelast
                            cito_price = last
                    else:
                        price = prelast
                        cito_price = last
                    if code == 'B050' or code == 'B049' or code == 'B035':
                        cito = 1
                        cito_price = price
                    elif cito_price != '':
                        try:
                            int(cito_price)
                            cito = 1
                        except ValueError:
                            cito_price = ''
                            cito = '0'
                    else:
                        cito_price = ''
                        cito = '0'
                else:
                    if self._round_needed:
                        try:
                            if last.endswith('.5'):
                                price = int(round(float(last) + 0.5))
                            else:
                                price = int(round(float(last)))
                        except ValueError:
                            price = last
                    else:
                        price = last
                    if code in ('B050', 'B049', 'B035'):
                        cito_price = price
                        cito = 1
                    else:
                        cito_price = ''
                        cito = '0'
                temp_list = [code, price, cito, cito_price] if self._b2b_price else [code, price, cito_price]
                list_to_return.append(temp_list)
                codes.append(code)
        self._check_list_for_uniqueness(codes_list=codes)
        return list_to_return

    def run(self):
        self._list_to_excel(self._convert_list_for_upload())
