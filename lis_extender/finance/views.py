from .forms import PriceConverterForm, CmdBillCompareForm, ExternalPriceForm, SalesReportForm
from .converter_engine import LISExcelCovnerter
from mainapp.excel_operations import input_name_to_output_name, DoubleCodeError
from .cmd_bill_engine import CMDCompairer, input_name_to_output_name_bill, date_convert
from .ext_price_converter_engine import ExternalPriceConverter
from .calculations import FinancialReport, color_generator
from bags.views import LpuList, LpuDetail

import pyodbc
import datetime

from django.contrib import messages
from django.http import HttpResponse
from django.utils.http import urlquote
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import PermissionRequiredMixin



class ALConverter(FormView):
    form_class = PriceConverterForm
    template_name = "finance/al_form.html"

    def form_valid(self, form):
        file_name = form.cleaned_data['excel_file']
        name = input_name_to_output_name(file_name)
        try:
            lc = LISExcelCovnerter(**form.cleaned_data)
            response = HttpResponse(lc.output_excel.getvalue(), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = f'attachment; filename={urlquote(name)}'
            return response
        except DoubleCodeError as dce:
            messages.error(self.request, f'Были обнаружены дублирующиеся записи для услуг: {dce}')
        except TypeError:
            service_code = form.cleaned_data.get('service_code')
            messages.error(self.request, f'Код исследования {service_code} не найден в файле')
        return self.render_to_response(self.get_context_data(form=form))


class BillCompareView(FormView):
    form_class = CmdBillCompareForm
    template_name = "finance/cmd_bill_form.html"

    def form_valid(self, form):
        file_name = form.cleaned_data.get('excel_file')
        name = input_name_to_output_name_bill(file_name)
        try:
            file = form.cleaned_data.get('excel_file')
            start_date = date_convert(str(form.cleaned_data.get('start_date')))
            end_date = date_convert(str(form.cleaned_data.get('end_date'))) + ' 23:59:59'
            buffer = CMDCompairer(excel_file=file,
                                  start_date=start_date, end_date=end_date)
            response = HttpResponse(buffer.output_excel.getvalue(), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = f'attachment; filename={urlquote(name)}'
            return response
        except pyodbc.ProgrammingError:
            messages.error(self.request, f'Не удается подключиться к БД ЛИС. Обратитесь к администратору')
            return self.render_to_response(self.get_context_data(form=form))
        except:
            messages.error(self.request, f'Что-то пошло не так')
            return self.render_to_response(self.get_context_data(form=form))


class ExternalPriceConverterForm(FormView):
    form_class = ExternalPriceForm
    template_name = 'finance/external_price_converter.html'

    def form_valid(self, form):
        clean_form = form.cleaned_data
        file = clean_form.get('excel_file')
        ext_service_code = clean_form.get('ext_service_code')
        lab = clean_form.get('lab')
        filename = input_name_to_output_name(file)
        try:
            buffer = ExternalPriceConverter(lab=lab, ext_code=ext_service_code, excel_file=file)
            response = HttpResponse(buffer.output_excel.getvalue(), content_type='application/vnd.ms-excel')
            response['Content-Disposition'] = f'attachment; filename={urlquote(filename)}'
            return response
        except TypeError:
            messages.error(self.request, f'Код исследования {ext_service_code} не найден в файле')
        return self.render_to_response(self.get_context_data(form=form))


class CustomersListView(LpuList):
    template_name = 'finance/customers_list.html'


class CustomerDetailView(LpuDetail):
    template_name = 'finance/customer_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = datetime.datetime.now()
        monday_date = today - datetime.timedelta(days=today.weekday())
        start_date = monday_date - datetime.timedelta(weeks=4)
        fr = FinancialReport(lpu=self.object, start_date=start_date,
                             final_date=monday_date, group_by='week')
        ws = FinancialReport(lpu=self.object, start_date=monday_date,
                             final_date=datetime.datetime.now(),
                             group_by='day')
        fr.group_sales()
        ws.group_sales()
        context['sales_by_weeks'] = fr.grouped_sales
        context['week_sales'] = ws.grouped_sales
        return context


class SalesReport(FormView):
    template_name = 'finance/sales_report.html'
    form_class = SalesReportForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        lpus = cleaned_data.get('lpus')
        start_date = cleaned_data.get('start_date')
        end_date = cleaned_data.get('end_date')
        grouping = cleaned_data.get('grouping')
        response_as = cleaned_data.get('response_as')
        common_trends = cleaned_data.get('common_trends')
        try:
            reports_to_response = {}
            reports_list = []
            if lpus.count() == 0:
                lpus = [None, ]
            for lpu in lpus:
                report = FinancialReport(lpu=lpu, start_date=start_date, final_date=end_date,
                                         group_by=grouping)
                report.group_sales()
                reports_list.append({'lpu': lpu, 'color': color_generator(), 'report': report.grouped_sales})
            reports_to_response['reports'] = reports_list
            if common_trends and lpus.count() > 0:
                common_report = FinancialReport(start_date=start_date, final_date=end_date,
                                                group_by=grouping)
                common_report.group_sales()
                reports_list.append({'lpu': None, 'color': None, 'report': common_report.grouped_sales})
            if response_as == 'tables':
                reports_to_response['tables'] = True
            if grouping == 'day':
                reports_to_response['by_day'] = True
            return self.render_to_response(self.get_context_data(form=form, **reports_to_response))
        except ValueError:
            messages.error(self.request, 'Дата начала должна быть меньше даты окончания!')
            return self.render_to_response(self.get_context_data(form=form))
