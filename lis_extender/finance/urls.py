from django.urls import path
from .views import ALConverter, BillCompareView, ExternalPriceConverterForm, CustomersListView, CustomerDetailView, \
    SalesReport

urlpatterns = [
    path('price_converter/', ALConverter.as_view(), name='price_converter'),
    path('cmd_bill_compare/', BillCompareView.as_view(), name='bill_compare'),
    path('external_price_converter/', ExternalPriceConverterForm.as_view(), name='ext_price_converter'),
    path('customers/', CustomersListView.as_view(), name='Customers'),
    path('customers/<int:pk>/', CustomerDetailView.as_view(), name='Customer details'),
    path('sales_report/', SalesReport.as_view(), name='Sales report')
]
