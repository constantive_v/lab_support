import pyodbc
from django.conf import settings
from django.core.management.base import BaseCommand
from analyzes.management.commands import HandleCatalog
from lis_extender.decorators import log_exceptions
from finance.models import Sale
from django.utils import timezone

LIS_DB = settings.LIS_DB
LOG_PATH = settings.LOG_PATH


server = LIS_DB.get('server')
database = LIS_DB.get('database')
username = LIS_DB.get('username')
password = LIS_DB.get('password')
IMPORT_CATALOG_LOG = 'import_sales.log'
WHERE = " where re.create_date > DATEADD(day, -1, GETDATE()) "


class HandleSales(HandleCatalog):
    table_name = ' Request re join InvoiceElement ie on ie.request_id = re.id join service_targets st on st.service_id = ie.service_id join Target ta on st.target_id = ta.id '
    params = ('id', 'creation_date', 'request_id', 'analysis_id', 'service_price', 'lpu_id')
    where = WHERE
    table_columns = (
    'ie.id', 're.create_date', 're.id', 'ta.id', 'isnull(ie.price_without_discount,0)', 're.hospital_id',)
    model = Sale

    def __init__(self, *args, **kwargs):
        if kwargs.get('initial'):
            where = ''
        else:
            where = self.where
        self.sql = 'select distinct ' + ', '.join(self.table_columns) + ' from ' + self.table_name + ' ' + f'{where}'

    @log_exceptions
    def get_catalog_data(self):
        self.catalog_data = []
        cnxn = pyodbc.connect(
            'DRIVER={ODBC Driver 17 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
        cursor = cnxn.cursor()
        cursor.execute(self.sql)
        row = cursor.fetchone()
        while row:
            mod_row = {}
            for num, value in enumerate(self.params):
                row_value = row[num]
                if num in (0, 2, 3, 5):
                    row_value = int(row_value)
                elif num == 1:
                    row_value = timezone.make_aware(row_value)
                mod_row[value] = row_value
            self.catalog_data.append(mod_row)
            row = cursor.fetchone()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--initial',
            action='store_true',
            help='Initial load of services',
        )

    def handle(self, *args, **options):
        if options['initial']:
            hs = HandleSales(initial=True)
        else:
            hs = HandleSales()
        hs.start()
        hs.join()
