from analyzes.models import Analysis
from bags.models import LPU
from finance.models import Sale
from django.core.management.base import BaseCommand
from random import choice, randint, randrange
import datetime
import csv
from django.utils import timezone


class SalesGenerator:
    def __init__(self, start_date, sales_num, prices_file):
        self.csv_price = prices_file
        self.price_list = None
        self.start_date = start_date
        self.sales_num = int(sales_num)
        self.analyzes = Analysis.objects.all()
        self.lpus = LPU.objects.all()

    def read_price(self):
        list_to_return = []
        with open(self.csv_price, mode='r', encoding='utf8') as csvf:
            opened_csv = csv.reader(csvf, delimiter=';')
            for row in opened_csv:
                if row[1] and row[1] != '':
                    try:
                        row[1] = int(row[1])
                        list_to_return.append(row)
                    except:
                        pass
        self.price_list = list_to_return

    def generate_date(self):
        start_date = timezone.make_aware(datetime.datetime.strptime(self.start_date, '%d.%m.%Y'))
        end_date = timezone.now()
        return start_date + datetime.timedelta(
            seconds=randint(0, int((end_date - start_date).total_seconds())),
        )

    def generate_request_id(self):
        dt = datetime.datetime.now()
        return dt.second + dt.microsecond + randint(100, 999)

    def gen_sales_istances(self):
        for n in range(self.sales_num):
            request_id = self.generate_request_id()
            lpu = choice(self.lpus)
            creation_date = self.generate_date()
            for _ in range(10):
                id = self.generate_request_id() + randint(10000, 99999)
                analysis_with_price = choice(self.price_list)
                analysis_code = analysis_with_price[0]
                analysis_price = analysis_with_price[1]
                try:
                    analysis = self.analyzes.get(code=analysis_code)
                except Analysis.DoesNotExist:
                    analysis = choice(self.analyzes)
                sl = Sale(id=id, creation_date=creation_date, request_id=request_id,
                          analysis=analysis, service_price=analysis_price,
                          lpu=lpu)
                sl.save()

    def run(self):
        self.read_price()
        self.gen_sales_istances()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--start_date',
            action='store',
            help='Start date in dd.mm.YYYY format'
        )
        parser.add_argument(
            '--sales_num',
            action='store',
            help='Number of sales instances'
        )
        parser.add_argument(
            '--file',
            action='store',
            help='Choose file with |Code|Price| in csv format'
        )

    def handle(self, *args, **options):
        start_date = options['start_date']
        sales_num = options['sales_num']
        file = options['file']
        sales_generator = SalesGenerator(start_date=start_date, sales_num=sales_num, prices_file=file)
        sales_generator.run()
