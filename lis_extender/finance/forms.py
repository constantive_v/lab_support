from django import forms
from django.conf import settings

from analyzes.forms import make_lab_choices
from bags.models import LPU

DATE_INPUT_FORMATS = settings.DATE_INPUT_FORMATS


class PriceConverterForm(forms.Form):
    excel_file = forms.FileField(required=True, label='Ваш прайс-лист')
    service_code = forms.CharField(max_length=5, required=True, initial='K001',
                                   label='Код услуги, по которому\nбудет определен столбец с кодами услуг',
                                   widget=forms.TextInput(attrs={'class': 'uk-input', 'placeholder': 'K001'}))
    round_prices = forms.BooleanField(label='Округлять цены', required=False,
                                      widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox'}))
    with_cito = forms.BooleanField(label='Прайс содержит цены на Cito услуги', required=False,
                                   widget=forms.CheckboxInput(attrs={'class': 'uk-checkbox'}))
    b2b_or_b2c = forms.ChoiceField(label='Тип прайса',
                                   widget=forms.Select(attrs={'class': 'js-select', 'placeholder': '---'}),
                                   choices=(
                                       ('B2B', 'B2B'),
                                       ('B2C', 'B2C')
                                   ), )
    pricelist_code = forms.CharField(label='Код прайс-листа', required=False,
                                     widget=forms.TextInput(attrs={'class': 'uk-input', 'autocomplete': 'off'}))

    def clean(self):
        cleaned_data = super().clean()
        b2b = cleaned_data.get('b2b_or_b2c')
        pricelist_code = cleaned_data.get('pricelist_code')
        if b2b == 'B2B' and pricelist_code == '':
            msg = 'Укажите код прайс-листа'
            self.add_error('pricelist_code', msg)

        excel_file = cleaned_data.get('excel_file')
        format_error = 'Требуется файл формата Excel 2010 и старше (xlsx)'
        if not excel_file.name.endswith('xlsx'):
            self.add_error('excel_file', format_error)
        elif excel_file.content_type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            self.add_error('excel_file', format_error)


class CmdBillCompareForm(forms.Form):
    excel_file = forms.FileField(required=True, label='Счет из CMD')
    start_date = forms.DateField(required=True, label='Дата регистрации заявок с',
                                 input_formats=DATE_INPUT_FORMATS,
                                 widget=forms.DateInput(attrs={'class': 'datepicker-here uk-input',
                                                               'autocomplete': 'off',
                                                               }))
    end_date = forms.DateField(required=True, label='Дата регистрации заявок по',
                               input_formats=DATE_INPUT_FORMATS,
                               widget=forms.DateInput(attrs={'class': 'datepicker-here uk-input',
                                                             'autocomplete': 'off',
                                                             }))

    def clean(self):
        cleaned_data = super().clean()
        excel_file = cleaned_data.get('excel_file')
        format_error = 'Требуется файл формата Excel 2010 и старше (xlsx)'
        if not excel_file.name.endswith('xlsx'):
            self.add_error('excel_file', format_error)
        elif excel_file.content_type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            self.add_error('excel_file', format_error)


class ExternalPriceForm(forms.Form):
    excel_file = forms.FileField(required=True, label='Прайс-лист другой лаборатории')
    ext_service_code = forms.CharField(max_length=20, required=True,
                                       label='Код услуги, по которому\nбудет определен столбец с кодами услуг\nдругой лаборатории',
                                       widget=forms.TextInput(attrs={'class': 'uk-input', 'autocomplete': 'off'}))
    lab = forms.ChoiceField(label='Лаборатория',
                            widget=forms.Select(attrs={'class': 'js-select-search', 'placeholder': '---'}),
                            choices=(make_lab_choices()), )

    def clean(self):
        cleaned_data = super().clean()
        excel_file = cleaned_data.get('excel_file')
        format_error = 'Требуется файл формата Excel 2010 и старше (xlsx)'
        if not excel_file.name.endswith('xlsx'):
            self.add_error('excel_file', format_error)
        elif excel_file.content_type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            self.add_error('excel_file', format_error)


class SalesReportForm(forms.Form):
    lpus = forms.ModelMultipleChoiceField(required=False, label='Выберите ЛПУ',
                                          queryset=LPU.objects.all(), widget=forms.SelectMultiple(
            attrs={'class': 'js-select-search',
                   'title': 'Выберите ЛПУ',}))
    start_date = forms.DateField(required=True, label='Начало периода',
                                 input_formats=DATE_INPUT_FORMATS,
                                 widget=forms.DateInput(attrs={'class': 'datepicker-here uk-input',
                                                               'autocomplete': 'off',
                                                               }))
    end_date = forms.DateField(required=True, label='Конец периода',
                               input_formats=DATE_INPUT_FORMATS,
                               widget=forms.DateInput(attrs={'class': 'datepicker-here uk-input',
                                                             'autocomplete': 'off',
                                                             }))
    grouping = forms.ChoiceField(required=False, label='Группировка по',
                                 widget=forms.Select(attrs={'class': 'js-select'}),
                                 choices=(
                                     ('', 'Без группировки'),
                                     ('day', 'Дню'),
                                     ('week', 'Неделе'),
                                     ('month', 'Месяцу'),
                                 ), )
    response_as = forms.ChoiceField(required=False, label='Отчет в виде',
                                    widget=forms.Select(attrs={'class': 'js-select'}),
                                    choices=(
                                        ('tables', 'Таблиц'),
                                        ('graphs', 'Графиков'),
                                    ), )
    common_trends = forms.BooleanField(required=False, label='Общие показатели продаж',
                                       widget=forms.CheckboxInput(attrs={
                                           'class': 'uk-checkbox',
                                       }))
