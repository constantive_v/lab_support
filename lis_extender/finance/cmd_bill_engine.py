# -*- coding: utf-8 -*-
import datetime

from openpyxl import load_workbook

from mainapp.excel_operations import ExcelConverter
from .cmd_bill_sql import get_request_list


class CMDCompairer(ExcelConverter):

    def __init__(self, excel_file, start_date, end_date):
        super().__init__(excel_file)
        self.converted_cmd_list = self.cmd_bill_convert()
        self.lq_input_list = self.lq_bill_convert(get_request_list(start_date=start_date, end_date=end_date))
        self.run()

    def _excel_to_list(self):
        list_to_return = list()
        wb = load_workbook(self._excel_file.file.file)
        ws = wb.worksheets[0]
        for row in ws:
            if 12 > len(row[1]) > 0:
                temp_list = [cell.value for cell in row]
                list_to_return.append(temp_list)
        return list_to_return

    def cmd_bill_convert(self):
        output_list = []
        for num, row in enumerate(self._excel_file):
            if row[0] == '':
                del self._excel_file[num][0]
        for row in self._excel_file:
            internal_nr = row[0]
            if internal_nr.startswith("'"):
                internal_nr = internal_nr[1:]
            if '.' in internal_nr:
                internal_nr = internal_nr[:internal_nr.index('.')]
            fio_full = row[1].upper()
            fi = fio_full[:fio_full.find(' ') + 2]
            tmp_list = row[2].split(',')
            for service in tmp_list:
                service_code = service[2:8]
                if len(service_code) == 6:
                    try:
                        int(service_code)
                        internal_nr_service = internal_nr + '-' + service_code
                        fi_service = fi + '-' + service_code
                        temp_list = [str(internal_nr), fio_full, str(service_code), internal_nr_service, fi_service, fi]
                        output_list.append(temp_list)
                    except ValueError:
                        pass
        return output_list

    def lq_bill_convert(self, lq_input_list):
        output_list = []
        for row in lq_input_list:
            internal_nr = row[0]
            fio_full = row[1].upper()
            fi = fio_full[:fio_full.find(' ') + 2]
            service = row[2]
            internal_nr_service = internal_nr + '-' + service
            fi_service = fi + '-' + service
            tmp_list = [internal_nr_service, fi_service, internal_nr, fio_full, service]
            output_list.append(tmp_list)
        return output_list

    def bill_compare(self):
        output_list = []
        lq_set = []
        lq_set_names = []
        cmd_set = []
        cmd_set_names = []
        cmd_list_2 = []
        for row in self.lq_input_list:
            nr_serv_code_lq = row[0]
            lq_set.append(nr_serv_code_lq)
        for row in self.converted_cmd_list:
            nr_serv_code_cmd = row[3]
            cmd_set.append(nr_serv_code_cmd)
        lq_set = set(lq_set)
        cmd_set = set(cmd_set)
        cmd_set.difference_update(lq_set)
        for row in self.converted_cmd_list:
            nr_serv_code_cmd = row[3]
            for nr_serv_code in cmd_set:
                if nr_serv_code == nr_serv_code_cmd:
                    cmd_list_2.append(row)
        for row in self.lq_input_list:
            fi_serv_lq = row[1]
            lq_set_names.append(fi_serv_lq)
        for row in cmd_list_2:
            fi_serv_cmd = row[4]
            cmd_set_names.append(fi_serv_cmd)
        lq_set_names = set(lq_set_names)
        cmd_set_names = set(cmd_set_names)
        cmd_set_names.difference_update(lq_set_names)
        for row in cmd_list_2:
            fi_serv_cmd = row[4]
            for fi_serv in cmd_set_names:
                if fi_serv == fi_serv_cmd:
                    output_list.append(row)
        return output_list

    def converted_cmd_list_to_usual_wo_remains(self):
        output_list = [['Повторяющиеся услуги в счете CMD:', '', ''],
                       ['№ заявки', 'ФИО', 'Код услуги CMD']]
        non_unique = [row for row in self._input_list if self._input_list.count(row) > 1]
        non_unique = set(non_unique)
        non_unique = list(non_unique)
        for row in non_unique:
            output_list.append(row[0:3])
        output_list.append(['', '', ''])
        output_list.append(['Не были найдены в выгрузке LQ:', '', ''])
        for row in self.converted_cmd_list:
            output_list.append(row[0:3])
        return output_list

    def run(self):
        self._list_to_excel(self.converted_cmd_list_to_usual_wo_remains())


def input_name_to_output_name_bill(file_name):
    file_name = file_name.name
    file_name = file_name.replace(' ', '_')
    reversed_string = file_name[::-1]
    dot_index = reversed_string.index('.') + 1
    without_extension = (reversed_string[dot_index::])[::-1]
    dte = datetime.datetime.now().strftime('%H.%M.%S')
    output_name = f'{without_extension}_сверка_{dte}.xlsx'
    return output_name


def date_convert(date):
    temp_date = date.split('-')
    output_date = ''.join(temp_date)
    return output_date
